function hist = criaHistograma2(alfabeto,fonte, nomeFich)
    hist=zeros(length(alfabeto),1);
    [~,~,ext]=fileparts(nomeFich);
        if ext=='.wav'
            for k=1:length(alfabeto)
            conta = 0;
            for x=1:2:length(fonte)
                if fonte(x)==alfabeto(k,1) && fonte(x+1)==alfabeto(k,2)
                    conta=conta+1;
                end
            end
            hist(k)= conta;                        
            end  
            bar(hist);
        end
        if ext=='.txt'
            for k=1:length(alfabeto)
            conta = 0;
            for x=1:2:length(fonte)
                if fonte(x)==alfabeto(k,1) && fonte(x+1)==alfabeto(k,2)
                    conta=conta+1;
                end
            end
            hist(k)= conta;                        
            end  
            bar(hist);
        end
        if ext=='.bmp' 
            for k=1:length(alfabeto)
                conta=0;
                for i=1:length(fonte(:,1))
                    for j=1:2:length(fonte(1,:))
                        if fonte(i,j)==alfabeto(k,1) && fonte(i,j+1)==alfabeto(k,2)
                            conta=conta+1;
                        end
                    end
                end
                hist(k)=conta;
            end
            bar(hist)
        end   
end