function hist = criaHistograma(alfabeto,fonte)
    hist=zeros(length(alfabeto),1);
    for k=1:length(alfabeto)
        hist(k)= length(fonte(fonte==alfabeto(k)));                           
    end  
    bar(hist);
end