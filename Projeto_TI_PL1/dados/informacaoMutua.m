function infoMutua=informacaoMutua(query,target,passo,alfabeto)
    posM=1;
    for k=1:passo:length(target)-length(query)+1
        res=0;
        target2=target(k:(length(query)+k-1));
        matriz=zeros(length(alfabeto));
          for i=1:length(query)
              indiceX=find(alfabeto==query(i));
              indiceY=find(alfabeto==target2(i));
              matriz(indiceX,indiceY)=matriz(indiceX,indiceY)+1;   
          end
          probConj=matriz./length(query);
          probY=sum(matriz)./length(target2);
          probX=sum(matriz,2)./length(query);
          for i=1:length(probConj)
              for j=1:length(probConj)
                if(probConj(i,j)~=0 && probY(j) ~= 0 && probX(i)~=0)
                    res = res + probConj(i,j)*log2(probConj(i,j)/(probX(i)*probY(j)));
                end
              end
          end
     infoMutua(posM)=res;
     posM=posM+1;
    end
end