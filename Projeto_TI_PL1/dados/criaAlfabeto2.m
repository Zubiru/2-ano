function [ver]=criaAlfabeto2(fonte, nomeFich)
    [~,~,ext]=fileparts(nomeFich);
    if  ext== '.wav'
        ver=[];
        pos=1;
        ver(pos,1)=fonte(1);
        ver(pos,2)=fonte(2);
        pos=2;
        for k=1:2:length(fonte)
        m=1;
            for x=1:length(ver(:,1))
                if fonte(k)==ver(x,1) && fonte(k+1)==ver(x,2)
                    m=0;    
                end
            end
             if m==1
                ver(pos,1)=fonte(k);
                ver(pos,2)=fonte(k+1);
                pos=pos+1;
            end
        end
    end
    if  ext== '.txt'
        ver=[];
        pos=1;
        ver(pos,1)=fonte(1);
        ver(pos,2)=fonte(2);
        pos=2;
        for k=1:2:length(fonte)
        m=1;
            for x=1:length(ver(:,1))
                
                if fonte(k)==ver(x,1) && fonte(k+1)==ver(x,2)
                    m=0;    
                end
            end
             if m==1
                ver(pos,1)=fonte(k);
                ver(pos,2)=fonte(k+1);
                pos=pos+1;
            end
        end
    end
    if ext=='.bmp'
        ver=[];
        pos=1;
        ver(pos,1)=fonte(1,1);
        ver(pos,2)=fonte(1,2);
        pos=2;
        
        for i=1:length(fonte(:,1))
           
            for j=1:2:length(fonte(1,:)) 
                m=1;
                for x=1:length(ver(:,1))

                    if fonte(i,j)==ver(x,1) && fonte(i,j+1)==ver(x,2)
                           m=0;
                    end
                end
                if m==1
                    ver(pos,1)=fonte(i,j);
                    ver(pos,2)=fonte(i,j+1);
                    pos=pos+1;
                end
            end
        end
    end
end