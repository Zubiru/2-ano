clc, clear, close all
%Ex1,2,3
nomeFicheiro='guitarSolo.wav';
[alfabeto,fonte]=criaAlfabeto(nomeFicheiro);
hist = criaHistograma(alfabeto,fonte);
entropia = criaEntropia(hist);
%Ex4
Hlen=hufflen(hist);
compMed = sum(hist .* Hlen) / sum(hist);
variancia = sum((Hlen-compMed).^2.*hist)/sum(hist);
%Ex5
alfabeto2=criaAlfabeto2(fonte,nomeFicheiro);
hist2 = criaHistograma2(alfabeto2,fonte,nomeFicheiro);
entropia2 = criaEntropia(hist2)/2;

%Ex6
%read Songs
 nomeSong01='Song01.wav';
 [fonteS1,fsS1]=audioread(nomeSong01);
 nomeSong02='Song02.wav';
 [fonteS2,fsS2]=audioread(nomeSong02);
 nomeSong03='Song03.wav';
 [fonteS3,fsS3]=audioread(nomeSong03);
 nomeSong04='Song04.wav';
 [fonteS4,fsS4]=audioread(nomeSong04);
 nomeSong05='Song05.wav';
 [fonteS5,fsS5]=audioread(nomeSong05);
 nomeSong06='Song06.wav';
 [fonteS6,fsS6]=audioread(nomeSong06);
 nomeSong07='Song07.wav';
 [fonteS7,fsS7]=audioread(nomeSong07);
%b)
%read Repeat and RepeatNoise 
  nomeFicheiro3='target02 - repeatNoise.wav';
  [fonte3,fs2]=audioread(nomeFicheiro3);
  nomeFicheiro2='target01 - repeat.wav';
  [fonte2, fs3]=audioread(nomeFicheiro2);
  
  infMutua=informacaoMutua(fonte,fonte2,length(fonte)*1/4,alfabeto);
  infMutua2=informacaoMutua(fonte,fonte3,length(fonte)*1/4,alfabeto);
  
  res=variacaoInfMutua(infMutua,infMutua2,1/fs2*length(fonte)*1/4);
%c)
vetorMax=[];

 infMutuaS1=informacaoMutua(fonte,fonteS1,length(fonte)*1/4,alfabeto);
 criaGrafico(infMutuaS1,1/fsS1*length(fonte)*1/4);
 vetorMax(1)=max(infMutuaS1);
 
 infMutuaS2=informacaoMutua(fonte,fonteS2,length(fonte)*1/4,alfabeto);
 criaGrafico(infMutuaS2,1/fsS2*length(fonte)*1/4);
 vetorMax(2)=max(infMutuaS2);
 
 infMutuaS3=informacaoMutua(fonte,fonteS3,length(fonte)*1/4,alfabeto);
 criaGrafico(infMutuaS3,1/fsS3*length(fonte)*1/4);
 vetorMax(3)=max(infMutuaS3);
 
 
 infMutuaS4=informacaoMutua(fonte,fonteS4,length(fonte)*1/4,alfabeto);
 criaGrafico(infMutuaS4,1/fsS4*length(fonte)*1/4);
 vetorMax(4)=max(infMutuaS4);
 
 
 infMutuaS5=informacaoMutua(fonte,fonteS5,length(fonte)*1/4,alfabeto);
 criaGrafico(infMutuaS5,1/fsS5*length(fonte)*1/4);
 vetorMax(5)=max(infMutuaS5);
 
 
 infMutuaS6=informacaoMutua(fonte,fonteS6,length(fonte)*1/4,alfabeto);
 criaGrafico(infMutuaS6,1/fsS6*length(fonte)*1/4);
 vetorMax(6)=max(infMutuaS6);
 
 
 infMutuaS7=informacaoMutua(fonte,fonteS7,length(fonte)*1/4,alfabeto);
 criaGrafico(infMutuaS7,1/fsS7*length(fonte)*1/4);
 vetorMax(7)=max(infMutuaS7);
 vetorDesc=sort(vetorMax,'descend');

