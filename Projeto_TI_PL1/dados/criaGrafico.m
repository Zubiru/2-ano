function criaGrafico(infMutua1, tempo)
    t=tempo;
    infMutua1(2,1)=t;
    res(1,1)=infMutua1(1,1);
    res(2,1)=infMutua1(2,1);
    for i=2:length(infMutua1)
        infMutua1(2,i)=infMutua1(2,i-1)+tempo;
        res(1,i)=infMutua1(1,i);
        res(2,i)=infMutua1(2,i);
    end
    plot(res(2,:),res(1,:));
    xlabel("Tempo (s)");
    ylabel("Inform��o M�tua");
end