function entropia = criaEntropia(hist)
    total=sum(hist);
    p=(hist/total);
    p(p==0)=[];
    l=-log2(p);
    r=p.*l;
    entropia = sum(r);
end