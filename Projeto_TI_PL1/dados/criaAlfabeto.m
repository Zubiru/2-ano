function [alfabeto,fonte]=criaAlfabeto(nomeFicheiro)
    [~,~,ext]=fileparts(nomeFicheiro);
    if ext=='.bmp'
        info=imfinfo(nomeFicheiro);
        alfabeto=(0:(2^info.BitDepth)-1);
        fonte = imread(nomeFicheiro);
    end
    if ext=='.txt'
        alfabeto=['a':'z','A':'Z'];
        abre = fopen(nomeFicheiro,'r');
        fonte = fscanf(abre,'%s');
    end
    if ext=='.wav'
        info=audioinfo(nomeFicheiro);
        d=2/2^info.BitsPerSample;
        alfabeto=(-1:d:1-d);
        [fonte,~]=audioread(nomeFicheiro);
    end
end
    