#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>

#define PIPE_NAME   "input_pipe"
#define BYTES_COMANDO 24

typedef struct{
	char nome[20];
	int numeroChegada;
	int tempoTriagem;
	int tempoAtendimento;
	int prioridade;
	int altTriagem;
	time_t tempo_chegada;
	time_t fimTriagem;
	time_t tempo_saida;
}paciente;

int fd;

void cleanup(){
	close(fd);
	exit(0);
}


int main()
{
	signal(SIGINT,cleanup);
	  
	  if ((fd=open(PIPE_NAME, O_WRONLY)) < 0)
	  {
	    perror("Não é possivel abrir a pipe para escrita! ");
	    exit(0);
	  }
	  int gerNome;
	  int gerGrupo;
	  int tmpTriagem;
	  int tPrioriade;
	  int tmpAtend;
	  
	  //Variaveis comando;
	  int fd_stdin;
	  fd_stdin=fileno(stdin);
	  fd_set readfds;
	  struct timeval tv;	
          int auxSelect;
          char input[BYTES_COMANDO];
	  char comando[BYTES_COMANDO];
	  paciente p;
	  // Do some work
	  while (1) {
	    //set p
	    memset(input,'\0',BYTES_COMANDO);
	    memset(comando,'\0',BYTES_COMANDO);
	    p.numeroChegada=1;
	    p.tempo_chegada=1;
	    p.fimTriagem=0;
	    p.tempo_saida=0;
	    p.altTriagem = 0;

	    FD_ZERO(&readfds);
	    FD_SET(fileno(stdin),&readfds);
 	    tv.tv_sec =1;
            tv.tv_usec =0;
	    fflush(stdout);
     	    auxSelect=select(fd_stdin + 1, &readfds, NULL, NULL,&tv);
	    if (auxSelect == -1) {
                        fprintf(stderr, "\nError in select : %s\n", strerror(errno));
                        exit(1);
                }
	    if(auxSelect==0){
	    	gerGrupo = rand() % 2; //gera numero entre 0 e 1
	    	tmpTriagem = rand() % 6 + 10; //gera numero entre 10 e 15
	    	tPrioriade = rand() % 5 + 1; //gera numero entre 1 e 5
	    	tmpAtend = rand() % 41 + 30; //gera numero entre 30 e 70
	    	if(gerGrupo==0){
			gerNome = rand() % 10; //gera numero entre 0 e 9
			switch (gerNome){
		    		case 0:
		        		strcpy(p.nome,"Antonio");
		        		break;
		    		case 1:
		        		strcpy(p.nome,"Miguel");
		        		break;
		    		case 2:
		        		strcpy(p.nome,"Diogo");
		        		break;
		    		case 3:
		        		strcpy(p.nome,"Maria");
		        		break;
		    		case 4:
		        		strcpy(p.nome,"Paula");
		        		break;
		    		case 5:
		        		strcpy(p.nome,"Joao");
		        		break;
		    		case 6:
		        		strcpy(p.nome,"Ines");
		        		break;
		    		case 7:
		        		strcpy(p.nome,"Gabriela");
		        		break;
		    		case 8:
		        		strcpy(p.nome,"Claudio");
		        		break;
		    		case 9:
		        		strcpy(p.nome,"Rute");
		        		break;
		    		default:
		        		break;
			}
	    	}
	    	else{ // gerGrupo==1
			int numGrupo;
			numGrupo = rand() % 7 + 2;
			strcpy(p.nome,"Grupo");
			p.numeroChegada = numGrupo;
		}
	    p.prioridade = tPrioriade;
	    p.tempoTriagem = tmpTriagem;
	    p.tempoAtendimento = tmpAtend;		
	    //printf("[ENVIAR] Paciente:%s %dms triagem %dms atendimento prioridade %d\n",p.nome,p.tempoTriagem,p.tempoAtendimento,p.prioridade);
	    write(fd, &p, sizeof(paciente));
	    }
	    else{ //select >0;
		p.altTriagem = 1;
		fgets(input,BYTES_COMANDO,stdin);
		int i=0;
		int set;
		char *aux=input;
		while(*aux!='-' ){
			if(*aux=='\n')
				break;
			comando[i]=*aux;
			aux++;
			i++;		
		}
		comando[i]='\0';
		aux++;
		if(strcmp(comando,"setTriagens")==0){		
			set=atoi(aux);
			p.altTriagem=set;
			write(fd,&p,sizeof(paciente));
				
		}
	    
		
	    }

	  }
	  return 0;
}
