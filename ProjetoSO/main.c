/* Autores:
Diogo Alexandre Cardoso Semedo 2016225626
Miguel Jorge Rasteiro Letra 2016226022
*/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <time.h>
#include <stdbool.h>
#include <semaphore.h>
#include <signal.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>

#define PIPE_NAME "input_pipe"
#define NMAXTRIAGENS 10
#define MAX 1000
#define TAMANHO_LOG 1024
#define NOME_SEMAFORO "semaforo1"
#define NOME_SEMAFORO_FILES "semaforo2"

int NumTriagens;
int NumDoctors;
int Shift_Length;
int MQ_MAX;

struct estatistica
{
	int numTotalPacientes;
	int numTotalAtendidos;
    	double tempoMedioAntes; // antes do inicio da triagem
    	double tempoMedioDepois; // entre o fim da triagem e o inicio do atendimento
    	double tempoMedioTotal; // desde que chegou ate sair
};


typedef struct
{
	char nome[20];
	int numeroChegada;
    	int tempoTriagem;
    	int tempoAtendimento;
    	int prioridade;
    	int altTriagem;
    	time_t tempo_chegada;
   	time_t fimTriagem;
   	time_t tempo_saida;
} paciente;

typedef struct
{
	long mtype;
	paciente pacienteP;
} mensagem;

struct queueList
{
    	paciente paciente;
    	struct queueList *prox;
};

//semaforos
pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;
sem_t full;
sem_t *sem;	//semaforo estatisticas
sem_t *semFiles; // semaforo para memory mapped files 


//memory mapped files
char *addr; // guarda inicio da memoria - memory mapped files
int *posicaoFicheiro; //guarda a posiçao na memoria
int shmidPosicao;

//flags ctrl-c e doutor temporário
volatile sig_atomic_t flag=0;
int flagDoutor=0;
pid_t pidDoutorTemp;

int msqid;
//Estatisticas na shared memory
int shmid;			
struct estatistica *Esta;



//Funcoes lista ligada
struct queueList* cria_Queue();
struct queueList *queue;
paciente removePrimeiro(struct queueList **li);
void insereLista(struct queueList **li,paciente paciente);

void sigintPai(int signum)
{
    	printf("Espere que os doutores terminem o paciente.\n");
    	flag=1;
}
void sigintFilho(int signum)
{
    	exit(0);
}

void sigEstatisticas(int signum)
{
	sem_wait(sem);
    	printf("Numero total pacientes triados: %d\n",Esta->numTotalPacientes);
    	printf("Numero total pacientes atendidos: %d\n",Esta->numTotalAtendidos);
    	printf("Tempo médio de espera antes do inicio da triagem: %lf\n",Esta->tempoMedioAntes);
    	printf("Tempo médio entre o fim da triagem e o inicio do atendimento: %lf\n",Esta->tempoMedioDepois);
   	printf("Média do tempo total: %lf\n",Esta->tempoMedioTotal);
    	sem_post(sem);
}

void sigusThread(int signum)
{
	
    	pthread_exit(NULL);
}

void ReadConfig(void)
{
    	FILE *file;
    	file = fopen("config.txt","r");
    	if(file==NULL)
    	{
        	printf("Não foi possivel aceder ao ficheiro.");
        	exit(1);
    	}
    	fscanf(file,"NumTriagens=%d\nNumDoctors=%d\nShift_Length=%d\nMQ_MAX=%d\n",&NumTriagens,&NumDoctors,&Shift_Length,&MQ_MAX);
    	fclose(file);
}

int ponteiroFicheiro(char *logs)
{
   	int x=0;
    	while(logs[x]!='\0')
    	{
        	x++;
    	}
    	memset(logs,'\0',TAMANHO_LOG);
    	return x;

}

void* Triagem(void *arg)
{
    	int id= *((int *)arg);
    	
	char logs[TAMANHO_LOG];
   	memset(logs,'\0',TAMANHO_LOG);


    	//Log inicio triagem
    	sem_wait(semFiles);
    	sprintf(logs,"Nova triagem [%d]\n",id);
    	memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
    	*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
    	sem_post(semFiles);

    	//sinal para diminuir threads
    	signal(SIGUSR2, sigusThread);
    	sigset_t block_ctrlc;
    	sigemptyset (&block_ctrlc);
    	sigaddset (&block_ctrlc, SIGUSR2);

    	paciente novoPaciente;
   	do
    	{
        	if(flag==1 && queue==NULL) // so sai quando a flag==1 e a lista ligada estiver vazia
        	{
            	sem_wait(semFiles);
            	sprintf(logs,"Fim da triagem [%d]\n",id);
            	memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
            	*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
            	sem_post(semFiles);
            	pthread_exit(NULL);
        	}

        	pthread_sigmask(SIG_BLOCK, &block_ctrlc, NULL);

        	/*retirar paciente da fila*/
        	pthread_mutex_lock(&mutex);
        	sem_wait(&full);
        	novoPaciente=removePrimeiro(&queue);
        	pthread_mutex_unlock(&mutex);
        	time_t tempoInicioTriagem=time(NULL);

        	//registar no log paciente
        	sem_wait(semFiles);
        	sprintf(logs,"Triagem do paciente %s\n",novoPaciente.nome);
        	memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
        	*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
       	 	sem_post(semFiles);

        	// executa triagem
        	usleep(novoPaciente.tempoTriagem*1000);
        	novoPaciente.fimTriagem=time(NULL);
        	
		//inserir fim de triagem no log
        	sem_wait(semFiles);
        	sprintf(logs,"Fim da triagem do paciente %s\n",novoPaciente.nome);
        	memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
        	*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
        	sem_post(semFiles);

        	//Estatisticas na shared memory
        	pthread_mutex_lock(&mutex);
        	(Esta->tempoMedioAntes)= ((Esta->tempoMedioAntes)*(Esta->numTotalPacientes)+(difftime(tempoInicioTriagem,novoPaciente.tempo_chegada)))/((Esta->numTotalPacientes)+1);
        	(Esta->numTotalPacientes)++;
        	pthread_mutex_unlock(&mutex);
	
        	//inserir na fila de mensagens
        	mensagem novaMensagem;
        	novaMensagem.mtype=novoPaciente.prioridade;
        	novaMensagem.pacienteP=novoPaciente;
        	if((msgsnd(msqid,&novaMensagem,sizeof(novaMensagem)-sizeof(long),0))<0)
        	{
            		perror("Envio na mensagem");
            		exit(1);
        	}
        	pthread_sigmask (SIG_UNBLOCK, &block_ctrlc, NULL);
    }
    while(1);


}
void Doctor()
{
	sem=sem_open(NOME_SEMAFORO,O_CREAT,0666,1); //Abrir semaforos criados no processo principal
    	semFiles=sem_open(NOME_SEMAFORO_FILES,O_CREAT,0666,1);
    	signal(SIGINT, sigintFilho);
    	sigset_t block_ctrlc;
    	sigemptyset (&block_ctrlc);
   	sigaddset (&block_ctrlc, SIGINT);

    	char logs[TAMANHO_LOG];
    	memset(logs,'\0',TAMANHO_LOG);

    	int tempoAtendimento;
    	//Iniciar a contagem do tempo
    	time_t tempoInicial=time(NULL);
    	time_t tempoFinal=tempoInicial+Shift_Length;

    	//PID
    	int id;
    	id=getpid();

    	//Escreve nos logs Inicio do turno
    	printf("Inicio do turno do doutor %d\n",id);
    	sem_wait(semFiles);
    	sprintf(logs,"Inicio do turno do doutor %d\n",id);
    	memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
    	*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
    	sem_post(semFiles);

    	sigprocmask (SIG_BLOCK, &block_ctrlc, NULL);
    	//Obter paciente fila de mensagens // usar semaforos
    	paciente novoPaciente;
    	mensagem mensagemRecebida;
    	while(time(NULL)< tempoFinal)
    	{
        	if((msgrcv(msqid,&mensagemRecebida,sizeof(mensagem),-5,IPC_NOWAIT))<0)
        	{
            		//perror("Receber Mensagem");
            		continue;

        	}
        	novoPaciente=mensagemRecebida.pacienteP;
		time_t tempoChegadaPaciente= time(NULL);
        
		//registar no log novo Paciente
        	sem_wait(semFiles);
        	sprintf(logs,"Atender novo paciente: %s ,Sou o doutor: %d\n",novoPaciente.nome,id);
        	memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
        	*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
        	sem_post(semFiles);


        	//aguardar o tempo predeterminado
        	tempoAtendimento=novoPaciente.tempoAtendimento;
        	usleep((tempoAtendimento)*1000);
        	novoPaciente.tempo_saida=time(NULL);
		
		//Regista nos logs Fim do atendimento do paciente
		sem_wait(semFiles);
        	sprintf(logs,"Acabei o paciente: %s ,Sou o doutor %d\n",novoPaciente.nome,id);
        	memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
        	*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
        	sem_post(semFiles);
        
		//Escrever nas Estatisticas na shared memory
        	sem_wait(sem);
        	(Esta->tempoMedioDepois)=((Esta->tempoMedioDepois)*(Esta->numTotalAtendidos)+(difftime(tempoChegadaPaciente,novoPaciente.fimTriagem)))/((Esta->numTotalAtendidos)+1);
        	(Esta->tempoMedioTotal)=((Esta->tempoMedioTotal)*(Esta->numTotalAtendidos)+(difftime(novoPaciente.tempo_saida,novoPaciente.tempo_chegada)))/((Esta->numTotalAtendidos)+1);
        	(Esta->numTotalAtendidos)++;
        	sem_post(sem);
	}
    	sigprocmask (SIG_UNBLOCK, &block_ctrlc, NULL);

}

void DoutorTemporario()
{
	sem=sem_open(NOME_SEMAFORO,O_CREAT,0666,1);
    	semFiles=sem_open(NOME_SEMAFORO_FILES,O_CREAT,0666,1);
    	signal(SIGINT, sigintFilho);
    	sigset_t block_ctrlc;
    	sigemptyset (&block_ctrlc);
    	sigaddset (&block_ctrlc, SIGINT);
    	struct msqid_ds dadosMQ;
	
	char logs[TAMANHO_LOG];
    	memset(logs,'\0',TAMANHO_LOG);

    	int tempoAtendimento;
    	//Iniciar a contagem do tempo
    	time_t tempoInicial=time(NULL);

    	//PID
    	pid_t id;
    	id=getpid();

	//print e escrita no ficheiro de log do inicio do turno do doutor temporário	
    	printf("Inicio do turno do doutor temporario %d\n",id);
    	sem_wait(semFiles);
       	sprintf(logs,"Inicio do turno do doutor temporário %d\n",id);
       	memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
      	*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
       	sem_post(semFiles);

	sigprocmask (SIG_BLOCK, &block_ctrlc, NULL);
   	
	//Obter paciente fila de mensagens
    	paciente novoPaciente;
    	mensagem mensagemRecebida;
    	msgctl(msqid,IPC_STAT,&dadosMQ);
    	
	while(dadosMQ.msg_qnum >= 0.8*MQ_MAX)
    	{
        	if((msgrcv(msqid,&mensagemRecebida,sizeof(mensagem),-5,0))<0)
        	{
			//perror("Receber Mensagem");
            		continue;
        	}
        	novoPaciente=mensagemRecebida.pacienteP;
        	
		//registar no log atendimento do paciente
		sem_wait(semFiles);
        	sprintf(logs,"Atender novo paciente: %s ,Sou o doutor %d -Temporário\n",novoPaciente.nome,id);
        	memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
        	*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
        	sem_post(semFiles);

		//aguardar o tempo predeterminado
        	tempoAtendimento=novoPaciente.tempoAtendimento;
        	usleep(tempoAtendimento*1000);
        	novoPaciente.tempo_saida=time(NULL);

		//Regista nos logs Fim do atendimento do paciente
		sem_wait(semFiles);
        	sprintf(logs,"Acabei o paciente: %s ,Sou o doutor %d -Temporário\n",novoPaciente.nome,id);
        	memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
        	*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
        	sem_post(semFiles);
        
		//Escrever nas Estatisticas na shared memory
        	sem_wait(sem);
        	(Esta->tempoMedioDepois)=((Esta->tempoMedioDepois)*(Esta->numTotalAtendidos)+(difftime(tempoInicial,novoPaciente.fimTriagem)))/((Esta->numTotalAtendidos)+1);
        	(Esta->tempoMedioTotal)=((Esta->tempoMedioTotal)*(Esta->numTotalAtendidos)+(difftime(novoPaciente.tempo_chegada,novoPaciente.tempo_saida)))/((Esta->numTotalAtendidos)+1);
        	(Esta->numTotalAtendidos)++;
        	sem_post(sem);

        	msgctl(msqid,IPC_STAT,&dadosMQ);
    	}
    	sigprocmask (SIG_UNBLOCK, &block_ctrlc, NULL);
}

int main(void)
{
    	ReadConfig();
    	int i;
    	signal(SIGINT, sigintPai);

	//Criar NamedPipe
    	if((mkfifo(PIPE_NAME,O_CREAT|O_EXCL|0600)<0) && (errno!=EEXIST))
    	{
        	perror("Erro criar pipe!");
        	exit(1);
    	}
	//Abrir pipe
    	int fd;
    	if((fd=open(PIPE_NAME,O_RDONLY))<0) // O_RDONLY PROVAVELMENTE ACRESCENTAR NON_BLOCK (O_RDONLY|O_NON_BLOCK)
    	{
		perror("Erro abrir pipe modo leitura");
        	exit(1);
    	}
	//Criar Queue e Message
    	queue=cria_Queue();
    	msqid=msgget(IPC_PRIVATE,IPC_CREAT|0777);
    	if(msqid<0)
    	{
        	perror("Erro na message queue");
        	exit(1);
    	}
	//criar Memory mapped files
    	int fLogs;
    	if((fLogs=open("Logs.txt",O_CREAT|O_RDWR,0666))<0)
    	{
        	perror("Erro a abrir ficheiro de log");
       	 	exit(1);
    	}
    	int tamanhoFicheiro=1024*100;
    	lseek(fLogs,tamanhoFicheiro,SEEK_SET);
    	write(fLogs,"",1);
   	if((addr=mmap(NULL,tamanhoFicheiro,PROT_READ|PROT_WRITE,MAP_SHARED,fLogs,0))==MAP_FAILED)
    	{
        	perror("Erro no mmap");
        	exit(1);
    	}

    	shmidPosicao=shmget(IPC_PRIVATE,sizeof(int),IPC_CREAT|0766);
    	if(shmidPosicao<0)
    	{
        	perror("Erro na criação da memoria partilhada");
        	exit(1);
    	}
    	posicaoFicheiro=(int *)shmat(shmidPosicao,NULL,0);
    	if(posicaoFicheiro<0)
    	{
        perror("Erro no atatch da memoria");
        exit(1);
    	}
    	*posicaoFicheiro=0;

	//shared memory
    	shmid=shmget(IPC_PRIVATE,sizeof(struct estatistica),IPC_CREAT|0766);
    	if(shmid<0)
    	{
        	perror("Erro na criação da memoria partilhada");
        	exit(1);
    	}
    	Esta=(struct estatistica*)shmat(shmid,NULL,0);
    	if(Esta<0)
    	{
        	perror("Erro no atatch da memoria");
        	exit(1);
    	}
    	Esta->numTotalAtendidos=0;
    	Esta->numTotalPacientes=0;
    	Esta->tempoMedioAntes=0.0;
    	Esta->tempoMedioDepois=0.0;
    	Esta->tempoMedioTotal=0.0;
	//Criar semafororos

    	if((sem=sem_open(NOME_SEMAFORO,O_CREAT,0666,1))==SEM_FAILED)
	{
		perror("Erro na abertura do semaforo!\n");
		exit(1);	
	}
	if((semFiles=sem_open(NOME_SEMAFORO_FILES,O_CREAT,0666,1))==SEM_FAILED)
	{
		perror("Erro na abertura do semaforo!\n");
		exit(1);
	}
    	if(sem_init(&full, 0, 0)<0)
	{
		perror("Erro no semaforo!\n");
		exit(1);
	}   


	//Criar threads
    	int t;
    	int ids[NMAXTRIAGENS];
    	pthread_t threads[NMAXTRIAGENS];
    	for(t=0; t<NumTriagens; t++)
    	{
        	ids[t] = t;
        	if(pthread_create(&threads[t],NULL,Triagem,&ids[t])!=0)
        	{
            		perror("Erro na criação das threads");
            		exit(0);
        	}
    	}
	//Criar processos
    	pid_t id1;
    	pid_t id2;
    	pid_t idTemp;
    	for(i=0; i < NumDoctors; i++)
    	{
        	id1=fork();
        	if(id1==0)
        	{
            		Doctor();
            		exit(0);

        	}
        	else if(id1<0)
        	{
            		perror("Erro na criação dos processos");
            		exit(1);
        	}
    	}
    	int idPaciente=2017001;
    	struct msqid_ds dadosMQ;
    	sigset_t block_ctrlc;
    	sigemptyset (&block_ctrlc);
    	sigaddset (&block_ctrlc, SIGINT);
    	int idDoutores;
    	char logs[TAMANHO_LOG];
    	memset(logs,'\0',TAMANHO_LOG);
    	signal(SIGUSR1,sigEstatisticas);
    	while(1)
    	{
        	if(flag==1)
        	{

            		for(i=0; i<NumTriagens; i++)
            		{
                		pthread_join(threads[i],NULL);
            		}
            		if(flagDoutor==1)
            		{
               	 		waitpid(pidDoutorTemp,NULL,0);
                		printf("Saiu o Doutor temporário %d\n",pidDoutorTemp);
				sem_wait(semFiles);
                		sprintf(logs,"Fim do turno do doutor temporário: %d\n",pidDoutorTemp);
                		memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
                		*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
                		sem_post(semFiles);
            		}

            		for(i=0; i<NumDoctors; i++)
            		{
                		idDoutores=wait(NULL);
				printf("Saiu o Doutor %d\n",idDoutores);
               			sem_wait(semFiles);
                		sprintf(logs,"Fim do turno do doutor: %d\n",idDoutores);
                		memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
                		*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
                		sem_post(semFiles);

            		}
			//Verifica se a Message queue está vazia
			msgctl(msqid,IPC_STAT,&dadosMQ);
            		printf("MESSAGE QUEUE : %d\n",dadosMQ.msg_qnum);
			
			//cria doutor caso nao esteja vazia
			while(dadosMQ.msg_qnum>0){
				idDoutores=fork();
				if(idDoutores==0){
					printf("vou criar um novo\n");
					Doctor();
					exit(0);
				}else if(idDoutores<0){
					perror("Erro na criação do novo  processo doutor.");
                        		exit(1);	
				}
				else{
					wait(NULL);
					
					//Escreve fim do doutor 
					sem_wait(semFiles);
                    			sprintf(logs,"Fim do turno do doutor: %d\n",idDoutores);
                    			memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
                    			*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
                    			sem_post(semFiles);
					
					msgctl(msqid,IPC_STAT,&dadosMQ);
				}
			}
            		if(queue==NULL)
            		{
                		printf("Lista ligada ficou vazia!!!!\n");
            		}
            		msync(addr,tamanhoFicheiro,MS_SYNC);
            		if(munmap(addr,tamanhoFicheiro)<0)
            		{
                		perror("Erro unmap ");
            		}
            		//Fechar ficheiro de logs pipe e message queue
			close(fLogs);
            		msgctl(msqid,IPC_RMID,NULL);
            		close(fd);
			
			//limpar semaforos
            		sem_destroy(sem);
            		sem_destroy(&full);
            		sem_destroy(semFiles);
	
			printf("Numero total pacientes triados: %d\n",Esta->numTotalPacientes);
    			printf("Numero total pacientes atendidos: %d\n",Esta->numTotalAtendidos);
   			printf("Tempo médio de espera antes do inicio da triagem: %lf\n",Esta->tempoMedioAntes);
    			printf("Tempo médio entre o fim da triagem e o inicio do atendimento: %lf\n",Esta->tempoMedioDepois);
    			printf("Média do tempo total: %lf\n",Esta->tempoMedioTotal);


            		// Libertar memoria
            		printf("A libertar a memoria.\n");
            		if(shmdt(posicaoFicheiro)!=0)
            		{
               		 	perror("Erro no detatch da memoria");
                		exit(1);
            		}
            		if(shmctl(shmidPosicao,IPC_RMID,NULL)!=0)
            		{
               			perror("Erro na libertação da memoria");
                		exit(1);
            		}
            		if(shmdt(Esta)!=0)
            		{
                		perror("Erro no detatch da memoria");
                		exit(1);
            		}
            		if(shmctl(shmid,IPC_RMID,NULL)!=0)
            		{
                		perror("Erro na libertação da memoria");
                		exit(1);
            		}
			printf("Sucess!\n");
            		exit(0);

		}
        	else
        	{
            		sigprocmask (SIG_BLOCK, &block_ctrlc, NULL);
            		paciente novoPaciente;
            		read(fd,&novoPaciente,sizeof(paciente));
            		novoPaciente.tempo_chegada=time(NULL);
          
            		if(novoPaciente.altTriagem>=1) //foi utilizado comando para alterar o numero de triagens
            		{
                		if(novoPaciente.altTriagem>NumTriagens) // numero de triagens introduzido é maior que o numero atual de triagens
                		{
                    			for(t=NumTriagens; t<novoPaciente.altTriagem; t++)
                    			{
                        			ids[t] = t;
                        			if(pthread_create(&threads[t],NULL,Triagem,&ids[t])!=0)
                        			{
                            				perror("Erro na criação das threads(redimensiona)");
                            				exit(1);
                        			}


                   		 	}
                    			NumTriagens=novoPaciente.altTriagem;
                		}
                		else //numero de triagens introduzido é menor que o numero atual de triagens
                		{
                    			for(t=NumTriagens-1; t>=novoPaciente.altTriagem; t--)
                    			{
                        			
                        			if(pthread_kill(threads[t],SIGUSR2)!=0)
                        			{
                            				perror("Erro no cancelamento de threads(redimensiona-)");
                            				exit(1);

                        			}
						sem_wait(semFiles);
            					sprintf(logs,"Fim da triagem [%d]\n",ids[t]);
            					memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
            					*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
            					sem_post(semFiles);
						ids[t]=0;
                   			}
                    			NumTriagens=novoPaciente.altTriagem;

                		}



            		}
            		else
            		{
                		if(novoPaciente.numeroChegada>1) // significa que recebemos 1 grupo pela pipe
                		{
                    			for(int i=0; i<novoPaciente.numeroChegada; i++)
                    			{
                        			paciente novoPaciente2;
                        			sprintf(novoPaciente2.nome,"%d",idPaciente);
                        			novoPaciente2.tempoTriagem=novoPaciente.tempoTriagem;
                        			novoPaciente2.tempoAtendimento=novoPaciente.tempoAtendimento;
                        			novoPaciente2.prioridade=novoPaciente.prioridade;
                        			novoPaciente2.tempo_chegada=novoPaciente.tempo_chegada;
                        			idPaciente++;
                        			//Insere na lista(queue) e incrementa o semaforo
                        			insereLista(&queue,novoPaciente2);
                        			sem_post(&full);
                    			}
                		}
                		else
                		{
					//Insere na lista(queue) e incrementa o semaforo
                    			insereLista(&queue,novoPaciente);
                    			sem_post(&full);
                    			
                		}
            		}
            		for(i=0; i<=NumDoctors; i++)
            		{
               			pid_t aux;
                		aux=waitpid(-1,NULL,WNOHANG);
                		if(aux>0 && aux!=pidDoutorTemp)
                		{
					printf("Fim do turno do doutor: %d\n",aux);
                    			sem_wait(semFiles);
                    			sprintf(logs,"Fim do turno do doutor: %d\n",aux);
                    			memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
                    			*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
                    			sem_post(semFiles);
                    			id2=fork();
                    			if(id2==0)
                    			{
                        			Doctor();
                        			exit(0);
                    			}
                    			else if(id2<0)
                    			{
                        			perror("Erro na criação do novo  processo doutor.");
                        			exit(1);
                    			}

                		}
                		else if(aux>0 && aux==pidDoutorTemp)
                		{
                    			sem_wait(semFiles);
                    			sprintf(logs,"Fim do turno do doutor temporario: %d\n",aux);
                    			memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
                    			*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
                    			sem_post(semFiles);
                    			printf("Processo temporario saiu!\n");
                    			flagDoutor=0;
                		}
            		}
           		msgctl(msqid,IPC_STAT,&dadosMQ);
            		if(dadosMQ.msg_qnum>MQ_MAX)
            		{
                		if(flagDoutor==0)
                		{
                    			idTemp=fork();
                    			if(idTemp==0)
                    			{
                        			DoutorTemporario();
                        			exit(0);
                    			}
                    			else if(idTemp<0)
                    			{
                        			perror("Erro criação processo doutor temporario!");
                        			exit(1);
                    			}
					sem_wait(semFiles);
                    			sprintf(logs,"Criei um doutor temporário %d\n",idTemp);
                    			memcpy(addr+(*posicaoFicheiro),logs,TAMANHO_LOG);
                    			*posicaoFicheiro=*posicaoFicheiro+ponteiroFicheiro(logs);
                    			sem_post(semFiles);
                    			
					pidDoutorTemp=idTemp;
                    			flagDoutor=1;

                		}

            		}

        	}
        sigprocmask (SIG_UNBLOCK, &block_ctrlc, NULL);
    	}
}

struct queueList* cria_Queue()
{
	struct queueList* li= (struct queueList*) malloc(sizeof(struct queueList));
    	if(li!=NULL)
    	{
        	li=NULL;
    	}
    	return li;
}
void insereLista(struct queueList **li,paciente paciente)
{
    	struct queueList *novo=(struct queueList *) malloc(sizeof(struct queueList));
    	if(!novo)
    	{
       	 	perror("Sem memoria disponivel!");
    	}	
    	novo->paciente=paciente;
    	novo->prox=NULL;
    	if(*li==NULL)
    	{
        	*li=novo;
    	}
    	else
    	{
        	struct queueList *temp=*li;
        	while(temp->prox!=NULL)
        	{
            		temp=temp->prox;
        	}

        	temp->prox=novo;
    	}
}

paciente removePrimeiro(struct queueList **li)
{
    	struct queueList *temp = *li;
    	paciente paciente = temp->paciente;
    	*li=temp->prox;
    	free(temp);
    	return paciente;
}


