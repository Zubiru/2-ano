2ºAno de Licenciatura.

1. Projeto Multimédia, é um jogo em javascript na canvas.
2. ProjetoPOO, tem uma interface e é feito em Java.
3. ProjetoSO, feito em C, exercita threads, processos, semáforos.
4. Projeto_ATD, análise de séries temporais através do Matlab.
5. ProjetoSCC, projeto de simulação com interface em Java.
6. Projeto_TI_PL1, trabalho sobre entropia, redundância e informação mútua em Matlab.
7. Projeto_TI_PL2, implementação do algoritmo LZ77 em C++.
