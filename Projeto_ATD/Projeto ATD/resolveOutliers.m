function aux=resolveOutliers(data)
    media=mean(data);
    desvioPadrao=std(data);
    indices = find(abs(data-media)>3*desvioPadrao);
    if(~isempty(indices))
        aux=data;
    for k=1:length(indices)
            if(aux(indices(k))>media)
                aux(indices(k))= media+2.5*desvioPadrao;
            else
                aux(indices(k))= media-2.5*desvioPadrao;
            end
        end
    end
end
    
    
    