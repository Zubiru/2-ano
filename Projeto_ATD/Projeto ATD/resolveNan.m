function aux=resolveNan(t,data_values)
    ind= find(isnan(data_values));
    if(~isempty(ind))
        aux = data_values;
        for i=1:length(ind)
            t_strip=t(ind(i)-4:ind(i)-1);
            v_strip=aux(ind(i)-4:ind(i)-1);
            aux(ind(i))=interp1(t_strip,v_strip,t(ind(i)),'pchip','extrap');
            if(aux(ind(i))<0)
                 t_strip = t(ind(i)-2:ind(i)-1);
                 v_strip = aux(ind(i)-2:ind(i)-1);
                 p = polyfit(t_strip,v_strip,0);
                 aux(ind(i))=polyval(p,t(ind(i)));
             end
        end
    end
end