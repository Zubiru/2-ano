d = readtable("dataset_ATD_PL8.csv");
t = (1:height(d));
t=t';
data_values = table2array(d(:,2));
 
figure(1);
plot(t,data_values,'-');
title("S�rie Temporal-Original");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");

 
data_values_withoutNan = resolveNan(t,data_values);
figure(2);
subplot(2,1,1);
plot(t,data_values,'-');
title("S�rie Temporal-Original");
subplot(2,1,2);
plot(t,data_values,'-',t,data_values_withoutNan,'ro');
title("S�rie temporal-Sem Nan");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");

 
data=resolveOutliers(data_values_withoutNan);
figure(3);
subplot(2,1,1);
plot(t,data_values_withoutNan,'-o');
title("S�rie Temporal-Sem Nan");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");
subplot(2,1,2);
plot(t,data_values_withoutNan,'-',t,data,'ro');
title("S�rie Temporal-Sem Nan e Outliers");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");

 
%Ponto 2
%Tendencia Grau 0
data_g0=detrend(data,'constant'); %aproxima��o polinomial de grau 0, sem a componente de tendencia
tendencia0 = data-data_g0;
figure(4);
subplot(2,1,1);
plot(t,data,'-o',t,tendencia0,'-*');
title("S�rie Regularizada e Componente de Tend�ncia");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");
subplot(2,1,2);
plot(t,data_g0,'-o');
title("S�rie Sem Componente de Tend�ncia");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");

 
%Tendencia Grau 1
data_g1=detrend(data,'linear');
tendencia1 = data-data_g1;
figure(5);
subplot(2,1,1);
plot(t,data,'-o',t,tendencia1,'-*');
title("S�rie Regularizada e Componente de Tend�ncia");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");
subplot(2,1,2);
plot(t,data_g1,'-o');
title("S�rie Sem Componente de Tend�ncia");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");

 
%Tendencia Grau 7
data_g7=polyfit(t,data,7);
tendencia7 = polyval(data_g7,t);
data_g7=data-tendencia7;
figure(6);
subplot(2,1,1);
plot(t,data,'-o',t,tendencia7,'-*');
title("S�rie Regularizada e Componente de Tend�ncia");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");
subplot(2,1,2);
plot(t,data_g7,'-o');
title("S�rie Sem Componente de Tend�ncia");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");

 
 
tendencia=zeros(height(d),1);
for i=1:height(d)
    tendencia(i)=5*cos((2*pi/365*i)+3*pi/4)+tendencia0(i);
end
 
 
figure(7);
subplot(2,1,1);
plot(t,data,'-o',t,tendencia,'-*');
title("S�rie Regularizada e Componente de Tend�ncia");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");
data_r=data-tendencia;
subplot(2,1,2);
plot(t,data_r,'-o');
title("S�rie Sem Componente de Tend�ncia");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");

 
 
%Sazonalidade
m=adftest(data_r);
 
%Trimestral
% aux = repmat((1:92)',1,1);
% ho = repmat((1:91)',3,1);
% ho = [ho;aux];
 
%Mensal
mes30 = repmat((1:30)',1,1);
mes31 = repmat((1:31)',1,1);
mes28 = repmat((1:28)',1,1);
ho = [mes31;mes28;mes31;mes30;mes31;mes30;mes31;mes31;mes30;mes31;mes30;mes31];
 
%Semestral
% ho = repmat((1:182)',1,1);
% aux = repmat((1:183)',1,1);
% ho = [ho;aux];
 
sx = dummyvar(ho);
Bs1 = sx\data_r;
st1 = sx*Bs1;
 
data_st=data-st1;
 
figure(8);
subplot(2,1,1);
plot(t,data,'-',t,data_st,'-ko');
title("S�rie Regularizada sem Sazonalidade");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");
subplot(2,1,2);
plot(t,st1,'-o');
title("Sazonalidade");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");

 
irr = data_r-st1;
 
figure(9);
subplot(2,1,1);
plot(t,data,'-+',t,data-irr,'-*');
title("S�rie Regularizada sem Componente Irregular");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");
subplot(2,1,2);
grafico6=plot(t,irr,'-o');
title("Componente Irregular");
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");

 
teste=adftest(data);
teste2=adftest(st1);
 
 
y1=st1(1:31);
figure(10);
subplot(211);
autocorr(y1);
subplot(212);
parcorr(y1);

 
id_y1=iddata(y1,[],1,'TimeUnit','days');
opt1_AR=arOptions('Approach','ls');
na1_AR=15;
model1_AR = ar(id_y1,na1_AR,opt1_AR);
pcoef1_AR=polydata(model1_AR);
y1_AR=y1(1:na1_AR);
for k=na1_AR+1:31
    y1_AR(k)=sum(-pcoef1_AR(2:end)'.*flip(y1_AR(k-na1_AR:k-1)));
end
y1_AR2=repmat(y1_AR,11,1);
aux = y1_AR2(1:24);
y1_AR2= [y1_AR2;aux];
y1_ARf=forecast(model1_AR,y1(1:na1_AR),31-na1_AR);
y1_ARf2=repmat([y1(1:na1_AR);y1_ARf],11,1);
aux = [y1(1:na1_AR);y1_ARf(1:9)];
y1_ARf2=[y1_ARf2;aux];
figure(11);
plot(t,st1,'-+',t,y1_AR2,'-o',t,y1_ARf2,'-*');
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");
E1_AR = sum((st1-y1_AR2).^2);
 
tt=(1:730)';
tendenciaTeste=zeros(730,1);
for i=1:730
    tendenciaTeste(i)=5*cos((2*pi/365*i)+3*pi/4)+tendencia0(1);
end %tendencia
figure(12);
plot(t,data,'-+',tt,repmat(y1_AR2,2,1)+tendenciaTeste,'-o');
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");
 
 
%ARMA
opt1_ARMAX = armaxOptions('SearchMethod','auto');
na1_ARMA = 9;
nc1_ARMA = 3;
model1_ARMA=armax(id_y1,[na1_ARMA nc1_ARMA],opt1_ARMAX);
[pa1_ARMA,pb1_ARMA,pc1_ARMA]=polydata(model1_ARMA);
e = randn(31,1); %ruido branco
y1_ARMA= y1(1:na1_ARMA);
for k=na1_ARMA+1:31
    y1_ARMA(k)=sum(-pa1_ARMA(2:end)'.*flip(y1_ARMA(k-na1_ARMA:k-1)))+sum(pc1_ARMA'.*flip(e(k-nc1_ARMA:k)));
end
y1_ARMA2=repmat(y1_ARMA,11,1);
aux = y1_ARMA(1:24);
y1_ARMA2= [y1_ARMA2;aux];
y1_ARMAf=forecast(model1_ARMA,y1(1:na1_ARMA),31-na1_ARMA);
y1_ARMAf2=repmat([y1(1:na1_ARMA);y1_ARMAf],11,1);
aux = [y1(1:na1_ARMA);y1_ARMAf(1:15)];
y1_ARMAf2=[y1_ARMAf2;aux];
figure(13);
grafico10=plot(t,st1,'-+',t,y1_ARMA2,'-o',t,y1_ARMAf2,'-*');
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");
 
E1_ARMA = sum((st1-y1_ARMA2).^2);
 
figure(14);
plot(t,data,'-+',tt,repmat(y1_ARMA2,2,1)+tendenciaTeste,'-o');
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");

 
%ARIMA
adftest(data_values);
p1_ARIMA=14;
D1_ARIMA=0;
q1_ARIMA=3;
 
Md1=arima(p1_ARIMA,D1_ARIMA,q1_ARIMA);
EstMd1=estimate(Md1,data_values,'Y0',data_values(1:p1_ARIMA));
y1_ARIMA = simulate(EstMd1,365);
figure(15);
plot(t,data,'-+',t,y1_ARIMA,'-o');
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");
 
figure(16);
plot(t,data,'-+',tt,repmat(y1_ARIMA,2,1)+tendenciaTeste,'-o');
xlabel("Tempo(Dias)");
ylabel("Consumo(KWh)");

E1_ARIMA = sum((st1-y1_ARIMA).^2);
 
 


