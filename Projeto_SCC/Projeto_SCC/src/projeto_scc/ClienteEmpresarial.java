/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_scc;

/**
 *
 * @author migue
 */
public class ClienteEmpresarial extends Cliente {
    public ClienteEmpresarial(Simulador s) {
        super(s);
    }
    @Override
    public double tempoAtendimento(int flag){
        if(flag==1){ //Balcao empresarial
            return this.s.getAleatorio().normal(s.getMedia_atendimentoE(),s.getStreamServicoE(),s.getDesvioAtendimentoE());
        }
        return this.s.getAleatorio().normal(s.getMedia_atendimentoEG(),s.getStreamServicoEG(),s.getDesvio_oposto());
    }
    @Override
    public boolean getTipo(){
        return false;
    }

    
    
    
}
