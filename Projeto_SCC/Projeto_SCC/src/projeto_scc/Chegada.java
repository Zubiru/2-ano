/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_scc;

// Classe que representa a chegada de um cliente. Deriva de Evento.
public class Chegada extends Evento {
    boolean tipoCliente;
    //Construtor
    Chegada(double i, Simulador s,int seccao,boolean tipo) {
        super(i, s,seccao);
        tipoCliente=tipo;
    }
    // M�todo que executa as ac��es correspondentes � chegada de um cliente
    void executa(Servico serv) {
        // Coloca cliente no servi�o - na fila ou a ser atendido, conforme o caso
        //if com multiplos das medias
        if(s.isDistribuicaoChegada()){ //true = exponencial
            if(tipoCliente){
                serv.insereServico(new Cliente(s));
                s.insereEvento(new Chegada(s.getInstante() + s.getAleatorio().exponencial(s.getMedia_chegGeral(), s.getStreamChegadaG()), s,seccao,true));
            }
            else{
                serv.insereServico(new ClienteEmpresarial(s));
                s.insereEvento(new Chegada(s.getInstante() + s.getAleatorio().exponencial(s.getMedia_chegEmpresarial(), s.getStreamChegadaE()), s,seccao,false));
            }
        }else{
            if(tipoCliente){
                serv.insereServico(new Cliente(s));
                s.insereEvento(new Chegada(s.getInstante() + s.getAleatorio().normal(s.getMedia_chegGeral(),s.getStreamChegadaG(),s.getDesvio_geral()), s,seccao,true));
            }
            else{
                serv.insereServico(new ClienteEmpresarial(s));
                s.insereEvento(new Chegada(s.getInstante() + s.getAleatorio().normal(s.getMedia_chegEmpresarial(),s.getStreamChegadaE(),s.getDesvio_empresarial()), s,seccao,false));
            }
        }
            
        // Agenda nova chegada para daqui a Aleatorio.exponencial(s.media_cheg) instantes
    }

    // M�todo que descreve o evento.
    // Para ser usado na listagem da lista de eventos.
    public String toString() {
        return "Chegada em " + instante;
    }

    @Override
    Cliente getCliente() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
