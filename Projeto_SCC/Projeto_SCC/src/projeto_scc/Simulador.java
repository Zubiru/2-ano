/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_scc;

import javax.swing.JTextArea;

public class Simulador {

    // Rel�gio de simula��o - vari�vel que cont�m o valor do tempo em cada instante
    private double instante;
    // M�dias das distribui��es de chegadas e de atendimento no servi�o
    private double media_chegGeral, media_chegEmpresarial;
    // N�mero de clientes que v�o ser atendidos
    private int n_clientes;
    // Servi�o - pode haver mais do que um num simulador
    private Servico servicoGeral;
    private Servico servicoEmpresarial;
    // Lista de eventos - onde ficam registados todos os eventos que v�o ocorrer na simula��o
    // Cada simulador s� tem uma
    private ListaEventos lista;
    private Aleatorio aleatorio;
    private boolean modoEstatico;
    private double media_atendimentoG,media_atendimentoE,media_atendimentoGE,media_atendimentoEG,desvio_oposto,desvio_empresarial,desvio_geral;
    private int streamChegadaG, streamChegadaE, streamServicoG,streamServicoE,streamServicoGE,streamServicoEG,empregadosG,empregadosE;      
    private double desvioAtendimentoG, desvioAtendimentoE;
    private boolean distribuicaoChegada;
    private javax.swing.JTextArea areaTexto;
    private boolean condicaoFim;
    private int tempoFim;
    // Construtor
    public Simulador(boolean modoEstatico,boolean distribuicaoChegada,int valorCondicaoFim,int streamChegadaG,int streamChegadaE,int streamServicoG,int streamServicoE, int streamServicoEG, int streamServicoGE,double media_chegGeral,double media_chegEmpresarial,double media_atendimentoG,double media_atendimentoE,double media_atendimentoEG,double media_atendimentoGE,double desvio_geral,double desvio_empresarial,double desvio_oposto,int empregadosG,int empregadosE,javax.swing.JTextArea areaTexto, boolean  condicaoFim, double desvioAtendimentoG, double desvioAtendimentoE) {
        this.modoEstatico=modoEstatico;
        this.distribuicaoChegada=distribuicaoChegada;
        this.n_clientes=valorCondicaoFim;
        this.streamChegadaG=streamChegadaG;
        this.streamChegadaE=streamChegadaE;
        this.streamServicoG=streamServicoG;
        this.streamServicoE=streamServicoE;
        this.streamServicoEG=streamServicoEG;
        this.streamServicoGE=streamServicoGE;
        this.media_chegGeral = media_chegGeral;
        this.media_chegEmpresarial = media_chegEmpresarial;
        this.media_atendimentoG=media_atendimentoG;
        this.media_atendimentoE=media_atendimentoE;
        this.media_atendimentoEG=media_atendimentoEG;
        this.media_atendimentoGE=media_atendimentoGE;
        this.desvio_geral=desvio_geral;
        this.desvio_empresarial=desvio_empresarial;
        this.desvio_oposto=desvio_oposto;
        this.empregadosG=empregadosG;
        this.empregadosE=empregadosE;
        this.areaTexto=areaTexto;
        this.condicaoFim = condicaoFim; //false se for tempo, true se for Numero de Clientes
        this.tempoFim= valorCondicaoFim;
        this.desvioAtendimentoG = desvioAtendimentoG;
        this.desvioAtendimentoE = desvioAtendimentoE;
        instante = 0;      
        servicoGeral = new Servico(this,this.empregadosG,2); 
        servicoEmpresarial=new Servico(this,this.empregadosE,1);
        lista = new ListaEventos(this);
        aleatorio = new Aleatorio(new int[]{streamChegadaG, streamChegadaE, streamServicoG,streamServicoE,streamServicoGE,streamServicoEG}, new RandomGenerator());
        if(distribuicaoChegada){
            insereEvento(new Chegada(instante+aleatorio.exponencial(media_chegGeral, streamChegadaG), this,2,true)); //Geral
            insereEvento(new Chegada(instante+aleatorio.exponencial(media_chegEmpresarial, streamChegadaE), this,1,false)); //Empresarial
        }
        else{
            insereEvento(new Chegada(instante + aleatorio.normal(media_chegGeral,streamChegadaG,desvio_geral),this,2,true));
            insereEvento(new Chegada(instante + aleatorio.normal(media_chegEmpresarial,streamChegadaE,desvio_empresarial),this,1,false));
        }
    }

    public double getDesvioAtendimentoG() {
        return desvioAtendimentoG;
    }

    public double getDesvioAtendimentoE() {
        return desvioAtendimentoE;
    }
    // M�todo que insere o evento e1 na lista de eventos
    void insereEvento(Evento e1) {
        lista.insereEvento(e1);
    }

    // M�todo que actualiza os valores estat�sticos do simulador
    private void act_stats() {
        servicoGeral.act_stats();
        servicoEmpresarial.act_stats();
    }

    // M�todo que apresenta os resultados de simula��o finais
    private void relat() {
        areaTexto.append("Resultados Finais:\n\n");
        areaTexto.append("\tServiço Geral:\n\n");
        servicoGeral.relat();
        areaTexto.append("\tServiço Empresarial:\n\n");
        servicoEmpresarial.relat();
    }

    // M�todo executivo do simulador
    public void executa() {
        Evento e1;
        // Enquanto n�o atender todos os clientes
        if(condicaoFim){
            while (servicoGeral.getAtendidos()+ servicoEmpresarial.getAtendidos() < n_clientes) {
                //lista.print();  // Mostra lista de eventos - desnecess�rio; � apenas informativo
                e1 =(Evento)(lista.removeFirst());  // Retira primeiro evento (� o mais iminente) da lista de eventos
                instante = e1.getInstante();         // Actualiza rel�gio de simula��o
                act_stats();                         // Actualiza valores estat�sticos
                if(e1.getSeccao()==2){
                    e1.executa(servicoGeral);                 // Executa evento
                }
                else{
                    e1.executa(servicoEmpresarial);                 // Executa evento
                }
            };
            relat();  // Apresenta resultados de simula��o finais
        }
        else{
            while (instante < tempoFim) {
                //	lista.print();  // Mostra lista de eventos - desnecess�rio; � apenas informativo
                e1 =(Evento)(lista.removeFirst());  // Retira primeiro evento (� o mais iminente) da lista de eventos
                instante = e1.getInstante();         // Actualiza rel�gio de simula��o
                if(instante > tempoFim){
                    instante = tempoFim;
                    act_stats();
                    break;
                }
                act_stats();                         // Actualiza valores estat�sticos
                if(e1.getSeccao()==2){
                    e1.executa(servicoGeral);                 // Executa evento
                }
                else{
                    e1.executa(servicoEmpresarial);                 // Executa evento
                }
            };
            relat();  // Apresenta resultados de simula��o finais
            
        }
    }

    // M�todo que devolve o instante de simula��o corrente
    public double getInstante() {
        return instante;
    }
/*
    // M�todo que devolve a m�dia dos intervalos de chegada
    public double getMedia_cheg() {
        return media_cheg;
    }
*/
    public double getMedia_chegGeral(){
      return media_chegGeral;
    }
    public double getMedia_chegEmpresarial(){
      return media_chegEmpresarial;
    }

    public Aleatorio getAleatorio() {
        return aleatorio;
    }

    public boolean isModoEstatico() {
        return modoEstatico;
    }

    public double getMedia_atendimentoG() {
        return media_atendimentoG;
    }

    public double getMedia_atendimentoE() {
        return media_atendimentoE;
    }

    public double getMedia_atendimentoGE() {
        return media_atendimentoGE;
    }

    public double getMedia_atendimentoEG() {
        return media_atendimentoEG;
    }

    public double getDesvio_oposto() {
        return desvio_oposto;
    }

    public double getDesvio_empresarial() {
        return desvio_empresarial;
    }

    public double getDesvio_geral() {
        return desvio_geral;
    }

    public boolean isDistribuicaoChegada() {
        return distribuicaoChegada;
    }

    public int getStreamChegadaG() {
        return streamChegadaG;
    }

    public int getStreamChegadaE() {
        return streamChegadaE;
    }

    public int getStreamServicoG() {
        return streamServicoG;
    }

    public int getStreamServicoE() {
        return streamServicoE;
    }

    public int getStreamServicoGE() {
        return streamServicoGE;
    }

    public int getStreamServicoEG() {
        return streamServicoEG;
    }

    public Servico getServicoGeral() {
        return servicoGeral;
    }

    public Servico getServicoEmpresarial() {
        return servicoEmpresarial;
    }

    public ListaEventos getLista() {
        return lista;
    }

    public JTextArea getAreaTexto() {
        return areaTexto;
    }
    
    

}
