/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_scc;

/**
 *
 * @author User
 */
public class Cliente {
    protected Simulador s;
    public Cliente(Simulador s) {
        this.s=s;
    }
    public boolean getTipo(){
        return true;
    }
    public double tempoAtendimento(int flag){
        if(flag==2){ //seccao 2 ou seja Balcao Geral
         
            return this.s.getAleatorio().normal(s.getMedia_atendimentoG(),s.getStreamServicoG(),s.getDesvioAtendimentoG());
        }
        return this.s.getAleatorio().normal(s.getMedia_atendimentoGE(),s.getStreamServicoGE(),s.getDesvio_oposto());
    }

 
}
