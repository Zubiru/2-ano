/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_scc;

import java.util.*;

// Classe que representa um servi�o com uma fila de espera associada
public class Servico {
    
    private int numeroFuncionarios;
    private int estado; // Vari�vel que regista o estado do servi�o: 0 - livre; 1 - ocupado
    private int atendidos; // N�mero de clientes atendidos at� ao momento
    private double temp_ult, soma_temp_esp, soma_temp_serv; // Vari�veis para c�lculos estat�sticos
    private Vector<Cliente> fila; // Fila de espera do servi�o
    private Simulador s; // Refer�ncia para o simulador a que pertence o servi�o
    private int seccao;
    private int cenas123 = 0;
    // Construtor
    Servico(Simulador s, int estado1,int seccao) {
        this.s = s;
        this.seccao = seccao;
        fila = new Vector<Cliente>(); // Cria fila de espera
        estado = estado1; // Livre
        temp_ult = s.getInstante(); // Tempo que passou desde o �ltimo evento. Neste caso 0, porque a simula��o ainda n�o come�ou.
        atendidos = 0;  // Inicializa��o de vari�veis
        soma_temp_esp = 0;
        soma_temp_serv = 0;
        numeroFuncionarios=estado1;
    }

    // M�todo que insere cliente (c) no servi�o
    public void insereServico(Cliente c) {
        if(s.isModoEstatico()){
            if (estado > 0) { // Se servi�o livre,
                estado--;     // fica ocupado e
                // agenda sa�da do cliente c para daqui a s.getMedia_serv() instantes
                //
                s.insereEvento(new Saida(s.getInstante() + c.tempoAtendimento(seccao), s,seccao,c));
            } else {
                fila.addElement(c); // Se servi�o ocupado, o cliente vai para a fila de espera
            }
        }
        else{ //Dinamico
            if(seccao==1){ //BALCAO EMPRESARIAL
                if (estado > 0) {
                    estado--;     
                    s.insereEvento(new Saida(s.getInstante() + c.tempoAtendimento(seccao), s,seccao,c));
                } 
                else{
                    if(s.getServicoGeral().estado >0){ //Cliente oposto
                        s.getServicoGeral().estado--;
                        s.insereEvento(new Saida(s.getInstante() + c.tempoAtendimento(s.getServicoGeral().seccao), s,s.getServicoGeral().seccao,c));
                    }
                    else{
                        if(!c.getTipo() && s.getLista().pesquisaCliente(seccao)){
                            cenas123++;
                            s.getServicoGeral().fila.addElement(new Cliente(s));
                            s.insereEvento(new Saida(s.getInstante() + c.tempoAtendimento(seccao), s,seccao,c));
                        }
                        else{
                            fila.addElement(c);
                        }
                    }
                }
            }
            else{ //BALCAO GERAL
                if (estado > 0) { // Se servi�o livre,
                    estado--;     // fica ocupado e
                    s.insereEvento(new Saida(s.getInstante() + c.tempoAtendimento(seccao), s,seccao,c));
                } else {
                    if(s.getServicoEmpresarial().estado >0){ //cliente oposto
                        s.getServicoEmpresarial().estado--;
                        s.insereEvento(new Saida(s.getInstante() + c.tempoAtendimento(s.getServicoEmpresarial().seccao), s,s.getServicoEmpresarial().seccao,c));
                    }
                    else{
                        fila.addElement(c);
                    }
                }
            }
        }
    }

    // M�todo que remove cliente do servi�o
    public void removeServico() {
        atendidos++; // Regista que acabou de atender + 1 cliente
        if(s.isModoEstatico()){
            if (fila.isEmpty()) {
                estado++; // Se a fila est� vazia, liberta o servi�o
            }
            else{
                Cliente c = (Cliente)fila.firstElement();
                fila.removeElementAt(0);
                s.insereEvento(new Saida(s.getInstante() + c.tempoAtendimento(seccao), s,seccao,c)); 
            }
        }
        else{
            if(seccao==1){ //BALCAO EMPRESARIAL
                if(!fila.isEmpty()){
                    Cliente c = (Cliente)fila.firstElement();
                    fila.removeElementAt(0);
                    s.insereEvento(new Saida(s.getInstante() + c.tempoAtendimento(seccao), s,seccao,c));
                }
                else if(!s.getServicoGeral().fila.isEmpty()){
                    Cliente c = (Cliente)s.getServicoGeral().fila.firstElement();
                    s.getServicoGeral().fila.removeElementAt(0);
                    s.insereEvento(new Saida(s.getInstante() + c.tempoAtendimento(seccao), s,seccao,c));
                }
                else{
                    estado++;
                }
            }
            else{ //BALCAO GERAL tipo é sempre true
                if(!fila.isEmpty()){
                    Cliente c = (Cliente)fila.firstElement();
                    fila.removeElementAt(0);
                    s.insereEvento(new Saida(s.getInstante() + c.tempoAtendimento(seccao), s,seccao,c));
                }
                else if(!s.getServicoEmpresarial().fila.isEmpty()){
                    Cliente c = (Cliente)s.getServicoEmpresarial().fila.firstElement();
                    s.getServicoEmpresarial().fila.removeElementAt(0);
                    s.insereEvento(new Saida(s.getInstante() + c.tempoAtendimento(seccao), s,seccao,c));
                }
                else{
                    estado++;
                }
            }
        }
    }

    // M�todo que calcula valores para estat�sticas, em cada passo da simula��o ou evento
    public void act_stats() {
        // Calcula tempo que passou desde o �ltimo evento
        double temp_desde_ult = s.getInstante() - temp_ult;
        // Actualiza vari�vel para o pr�ximo passo/evento
        temp_ult = s.getInstante();
        // Contabiliza tempo de espera na fila
        // para todos os clientes que estiveram na fila durante o intervalo
        soma_temp_esp += fila.size() * temp_desde_ult;
        // Contabiliza tempo de atendimento
        soma_temp_serv += (numeroFuncionarios-estado) * temp_desde_ult; //depende do numero de funcionários???
    }

    // M�todo que calcula valores finais estat�sticos
    public void relat() {
        // Tempo m�dio de espera na fila
        double temp_med_fila = soma_temp_esp / (atendidos + fila.size());
        // Comprimento m�dio da fila de espera
        // s.getInstante() neste momento � o valor do tempo de simula��o,
        // uma vez que a simula��o come�ou em 0 e este m�todo s� � chamdo no fim da simula��o
        double comp_med_fila = soma_temp_esp / s.getInstante();
        // Tempo m�dio de atendimento no servi�o
        double utilizacao_serv = (soma_temp_serv / s.getInstante())/numeroFuncionarios;
        // Apresenta resultados
        s.getAreaTexto().append("\tTempo médio de espera: " + temp_med_fila +"\n");
        s.getAreaTexto().append("\tComp. médio da fila: " + comp_med_fila+"\n");
        s.getAreaTexto().append("\tUtilização do serviço: " + utilizacao_serv+"\n");
        s.getAreaTexto().append("\tTempo de simulação: " + s.getInstante()+"\n"); // Valor actual
        s.getAreaTexto().append("\tNúmero de clientes atendidos: " + atendidos+"\n");
        s.getAreaTexto().append("\tNúmero de clientes na fila: " + fila.size()+"\n\n"); // Valor actual
        if(cenas123!=0)
            s.getAreaTexto().append("\tNúmero de interrupções: " + cenas123+"\n\n");
    }

    // M�todo que devolve o n�mero de clientes atendidos no servi�o at� ao momento
    public int getAtendidos() {
        return atendidos;
    }

}
