/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_scc;

public class Saida extends Evento {
    private Cliente cliente;
    //Construtor
    Saida(double i, Simulador s,int seccao, Cliente c) {
        super(i, s,seccao);
        this.cliente=c;
    }

    // M�todo que executa as ac��es correspondentes � sa�da de um cliente
    void executa(Servico serv) {
        // Retira cliente do servi�o
        serv.removeServico();
    }

    // M�todo que descreve o evento.
    // Para ser usado na listagem da lista de eventos.
    public String toString() {
        return "Saida em " + instante;
    }
    @Override
    public boolean isSaida(){
        return true;
    }

    public Cliente getCliente() {
        return cliente;
    }
    
   
    
}
