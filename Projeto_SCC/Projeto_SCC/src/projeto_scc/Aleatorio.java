/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_scc;
import java.util.Random;

// Classe para gera��o de n�meros aleat�rios segundos v�rias distribui��es
// Apenas a distribui��o exponencial negativa est� definida
public class Aleatorio {
    private int [] streams;
    private double [] valores;
    private RandomGenerator genera;
    
    Aleatorio(int [] streams, RandomGenerator genera){
        
        this.streams=streams;
        this.genera = genera;
        valores = new double[streams.length];
        for(int i=0;i<streams.length;i++){
            valores[i]=-1;
        }
    }
    // Gera um n�mero segundo uma distribui��o exponencial negativa de m�dia m
    public double exponencial(double m,int stream) {
        //return m;
        return (-m * Math.log(genera.rand(stream)));
    }
    
    public double normal(double m,int stream,double dp){
        
        //return m;
        
        for(int i=0;i<streams.length;i++){
            if(streams[i]==stream && valores[i]!=-1){
                double x = valores[i];
                valores[i]=-1;
                return x;
            }
        }
        double w;
        double v1;
        double v2;
        do{
        double u1 = genera.rand(stream);
        double u2 = genera.rand(stream);
        
        v1= 2*u1-1;
        v2= 2*u2-1;
        
        w = Math.pow(v1, 2)+Math.pow(v2,2);
        }while(w>1);
        
        double y1= v1*Math.sqrt((-2*Math.log(w))/w);
        double y2= v2*Math.sqrt((-2*Math.log(w))/w);
        
        double x1= m+y1*dp;
        double x2= m+y2*dp;
        if(x1<0){
            x1=0;
        }
        if(x2<0){
            x2=0;
        }
        for(int i=0;i<streams.length;i++){
            if(streams[i]==stream){
                valores[i]=x2;
            }
        }
        return x1;
    }

    public int[] getStreams() {
        return streams;
    }

    public double[] getValores() {
        return valores;
    }

    
    public void abc() {
        for(int i=0;i<streams.length;i++){
            System.out.println(streams[i] +" "+ valores[i]);
        }
    }



}
