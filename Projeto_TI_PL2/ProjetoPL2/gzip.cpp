/*Author: Rui Pedro Paiva
Teoria da Informa��o, LEI, 2007/2008*/
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <string.h>
#include "gzip.h"
#include <conio.h>
#include <math.h>
#include "huffman.h"
#include <locale.h>
#define CCCC 19
#define MCL 15
#define NLIT 257
#define CD 30
//fun��o principal, a qual gere todo o processo de descompacta��o
void getData(unsigned long needBits,unsigned long *availBits,unsigned long *rb,unsigned int *nome,FILE* gzFile);
int converter(int bits);
void criarHuffman(char *Arraycomp,char** HuffmanArray,int tam_2,HuffmanTree* hft);
bool notContains(char *a, char b,int tam);
void criarTabela(unsigned long *availBits,unsigned long *rb,FILE* gzFile,HuffmanTree* hft,int num,char *arraycomp);
int comprimento(int comp,unsigned long *availbits,unsigned long *rb,FILE *gzfile);
int distancia(int comp,unsigned long *availbits,unsigned long *rb,FILE *gzfile);
void deflate(HuffmanTree *hftCOMPLIT, HuffmanTree *hftCODDIST,unsigned long *availbits,unsigned long *rb,FILE *gzfile, unsigned char *fichdeflate,int *indice);
int main(int argc, char** argv)
{
    setlocale(LC_ALL,"Portuguese");
    //--- Gzip file management variables
    FILE *gzFile;  //ponteiro para o ficheiro a abrir
    long fileSize;
    long origFileSize;
    int numBlocks = 0;
    gzipHeader gzh;
    unsigned char byte;  //vari�vel tempor�ria para armazenar um byte lido directamente do ficheiro
    unsigned long rb = 0;  //�ltimo byte lido (poder� ter mais que 8 bits, se tiverem sobrado alguns de leituras anteriores)
    unsigned long needBits = 0;
    unsigned long availBits = 0;
    int indice=0;


    //--- obter ficheiro a descompactar
    char fileName[] = "FAQ.txt.gz";
    /*
    if (argc != 2)
    {
        printf("Linha de comando inv�lida!!!");
        return -1;
    }
    char * fileName = argv[1];
    */
    //--- processar ficheiro
    gzFile = fopen(fileName, "rb");
    fseek(gzFile, 0L, SEEK_END);
    fileSize = ftell(gzFile);
    fseek(gzFile, 0L, SEEK_SET);

    //ler tamanho do ficheiro original (acrescentar: e definir Vector com s�mbolos
    origFileSize = getOrigFileSize(gzFile);


    //--- ler cabe�alho
    int erro = getHeader(gzFile, &gzh);
    if (erro != 0)
    {
        printf ("Formato inv�lido!!!");
        return -1;
    }

    //--- Para todos os blocos encontrados
    char BFINAL;
    char HDIST;
    char HLIT;
    char HCLEN;

    unsigned char fichdeflate[origFileSize];
    do
    {
        //--- ler o block header: primeiro byte depois do cabe�alho do ficheiro
        needBits = 3;
        if (availBits < needBits)
        {
            fread(&byte, 1, 1, gzFile);
            rb = (byte << availBits) | rb;
            availBits += 8;
        }

        //obter BFINAL
        //ver se � o �ltimo bloco
        BFINAL = rb & 0x01; //primeiro bit � o menos significativo
        printf("BFINAL = %d\n", BFINAL);
        rb = rb >> 1; //descartar o bit correspondente ao BFINAL
        availBits -=1;

        //analisar block header e ver se � huffman din�mico
        if (!isDynamicHuffman(rb)){
            printf("Estatico");
           continue;
        }  //ignorar bloco se n�o for Huffman din�mico
        rb = rb >> 2; //descartar os 2 bits correspondentes ao BTYPE
        availBits -= 2;


        //--- Se chegou aqui --> compactado com Huffman din�mico --> descompactar
        //**************************************************
        //****** ADICIONAR PROGRAMA... *********************
        //**************************************************

        //Leitura do HLIT, HDIST e HCLEN
        unsigned int aux;
        getData(5,&availBits,&rb,&aux,gzFile);
        HLIT=(unsigned int) aux;
        getData(5,&availBits,&rb,&aux,gzFile);
        HDIST=(unsigned int) aux;
        getData(4,&availBits,&rb,&aux,gzFile);
        HCLEN = (unsigned int) aux;

        printf("HLIT=%d\n",HLIT);
        printf("HDIST=%d\n",HDIST);
        printf("HCLEN=%d\n",HCLEN);

        //Leitura dos CCCC
        char ARRAYCOMP[CCCC];
        memset(ARRAYCOMP,0,sizeof(ARRAYCOMP));
        int ordem[]={16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};
        for(int i=0;i<HCLEN+4;i++){
            getData(3,&availBits,&rb,&aux,gzFile);
            ARRAYCOMP[ordem[i]]=(unsigned int) aux;
        }

        /*
        for(int i=0;i<19;i++){
            printf("COMP[%d]=%d\n",i,ARRAYCOMP[i]);
        }
        */
        //Array com os c�digos de Huffman correspondentes ao CCCC
        char **HuffmanArray = (char**)malloc(CCCC*sizeof(char*));
        for(int i=0;i<CCCC;i++){
            HuffmanArray[i]=(char *)malloc(MCL*sizeof(char));
            sprintf(HuffmanArray[i], "NULL");
        }
        HuffmanTree *hft = createHFTree();
        criarHuffman(ARRAYCOMP,HuffmanArray,CCCC,hft);
        /*
        for(int i=0;i<CCCC;i++){
            if(strcmp(HuffmanArray[i],"NULL")!=0){
                printf("COMP[%d]=%d | %s\n",i,ARRAYCOMP[i],HuffmanArray[i]);
            }
        }*/
        //Comprimento Lit

        char COMPLIT[HLIT+NLIT];
        memset(COMPLIT,0,(HLIT+NLIT)*sizeof(char));
        //Fun��o para leitura dos respetivos comprimentos relativos aos comprimentos de literais/comprimentos
        criarTabela(&availBits,&rb,gzFile,hft,HLIT+NLIT,COMPLIT);
        //Array com os c�digos de Huffman correspondentes ao c�digo de  comprimentos de literais/comprimentos
        char **HuffmanCOMPLIT = (char**)malloc((HLIT+NLIT)*sizeof(char*));
        for(int i=0;i<HLIT+NLIT;i++){
            HuffmanCOMPLIT[i]=(char *)malloc(MCL*sizeof(char));
            sprintf(HuffmanCOMPLIT[i], "NULL");
        }
        HuffmanTree *hftCOMPLIT = createHFTree();
        criarHuffman(COMPLIT,HuffmanCOMPLIT,HLIT+NLIT,hftCOMPLIT);
        /*
         for(int i=0;i<HLIT+NLIT;i++){
                printf("[%d]     %d          %s\n",i,COMPLIT[i],HuffmanCOMPLIT[i]);
        }
        */

        //C�digo Dist
        char CODDIST[CD];
        memset(CODDIST,0,CD*sizeof(char));
        //Fun��o para leitura dos respetivos comprimentos relativos aos c�digos de dist�ncias
        criarTabela(&availBits,&rb,gzFile,hft,HDIST+1,CODDIST);
        //Array com os c�digos de Huffman correspondentes aos c�digo de dist�ncias
        char **HuffmanCODDIST = (char**)malloc((HDIST+1)*sizeof(char*));
        for(int i=0;i<HDIST+1;i++){
            HuffmanCODDIST[i]=(char *)malloc(MCL*sizeof(char));
            sprintf(HuffmanCODDIST[i], "NULL");
        }
        HuffmanTree *hftCODDIST = createHFTree();
        criarHuffman(CODDIST,HuffmanCODDIST,HDIST+1,hftCODDIST);
        /*
        printf("--------------------COMP DIST-------------------\n");
        for(int i=0;i<HDIST+1;i++){
                printf("[%d]     %d          %s\n",i,CODDIST[i],HuffmanCODDIST[i]);
        }
        */
        deflate(hftCOMPLIT,hftCODDIST,&availBits,&rb,gzFile,fichdeflate,&indice);
        //for(int i=0;i<=indice;i++){
         //   printf("%c",fichdeflate[i]);
        //}

        //actualizar n�mero de blocos analisados
        numBlocks++;
    }while(BFINAL == 0);
    //escrita num ficheiro com o correspondete filename
    FILE *fp;
    fp=fopen(gzh.fName,"wb");
    fwrite(fichdeflate,sizeof(unsigned char),sizeof(fichdeflate),fp);
    fclose(fp);

    //termina��es
    fclose(gzFile);
    printf("End: %d bloco(s) analisado(s).\n", numBlocks);


    //teste da fun��o bits2String: RETIRAR antes de criar o execut�vel final
    //char str[8];
    //bits2String(str, 0b111);
    //printf("%s\n", str);


    //RETIRAR antes de criar o execut�vel final
    //return EXIT_SUCCESS;
    return 0;
}

void criarTabela(unsigned long *availBits,unsigned long *rb,FILE* gzFile,HuffmanTree* hft,int num,char *arraycomp){
        unsigned int bit;
        char comp;
        int esp;
        int j;
        char aux;
        for(int i=0;i<num;i++){
          resetCurNode(hft);
          do{
          getData(1,availBits,rb,&bit,gzFile);
          comp=nextNode(hft,bit+'0');
          if(comp==-1)
            printf("Erro muito grave\n");
          }while(comp==-2);  //leitura at� ser folha
          switch(comp){
            case 18:
                getData(7,availBits,rb,&bit,gzFile);
                esp=bit+11;
                for(j=i;j<esp+i;j++)
                    arraycomp[j]=0;
                i=i+esp-1;
                break;
            case 17:
                getData(3,availBits,rb,&bit,gzFile);
                esp=bit+3;
                for(j=i;j<esp+i;j++)
                    arraycomp[j]=0;
                i=i+esp-1;
                break;
            case 16:
                getData(2,availBits,rb,&bit,gzFile);
                esp=bit+3;
                 for(j=i;j<esp+i;j++)
                    arraycomp[j]=arraycomp[j-1];
                i=i+esp-1;
                break;
            default:
                arraycomp[i]=comp;
                break;
          }
        }
}

void deflate(HuffmanTree *hftCOMPLIT, HuffmanTree *hftCODDIST,unsigned long *availbits,unsigned long *rb,FILE *gzfile,unsigned char *fichdeflate,int *indice){
        unsigned int bit;
        int comp;
        int esp;
        int tamcomp;
        int tamdist;
        do{
            resetCurNode(hftCOMPLIT);
            resetCurNode(hftCODDIST);
            do{
                getData(1,availbits,rb,&bit,gzfile);
                comp=nextNode(hftCOMPLIT,bit+'0');
                if(comp==-1)
                    printf("Erro muito grave\n");
            }while(comp==-2);
            if(comp>256){
                tamcomp=comprimento(comp,availbits,rb,gzfile);
                do{
                    getData(1,availbits,rb,&bit,gzfile);
                    comp=nextNode(hftCODDIST,bit+'0');
                    if(comp==-1)
                        printf("Erro muito grave\n");
                }while(comp==-2);
                tamdist=distancia(comp,availbits,rb,gzfile);
                //algoritmo para andar para tras a distancia e copiar para a frente o comprimento
                int tamc;
                for(tamc=0;tamc<tamcomp;tamc++){
                    fichdeflate[*indice]=fichdeflate[(*indice)-tamdist];
                    (*indice)++;
                }

            }
            else if(comp<256){
                fichdeflate[*indice]=comp;
                (*indice)++;
            }

        }while(comp!=256);
}

int distancia(int comp,unsigned long *availbits,unsigned long *rb,FILE *gzfile){
    int num;
    unsigned int esp;
    switch(comp){
            case 0:
                num=1;
                break;
            case 1:
                num=2;
                break;
            case 2:
                num=3;
                break;
            case 3:
                num=4;
                break;
            case 4:
                getData(1,availbits,rb,&esp,gzfile);
                num=5+esp;
                break;
            case 5:
                getData(1,availbits,rb,&esp,gzfile);
                num=7+esp;
                break;
            case 6:
                getData(2,availbits,rb,&esp,gzfile);
                num=9+esp;
                break;
            case 7:
                getData(2,availbits,rb,&esp,gzfile);
                num=13+esp;
                break;
            case 8:
                getData(3,availbits,rb,&esp,gzfile);
                num=17+esp;
                break;
            case 9:
                getData(3,availbits,rb,&esp,gzfile);
                num=25+esp;
                break;
            case 10:
                getData(4,availbits,rb,&esp,gzfile);
                num=33+esp;
                break;
            case 11:
                getData(4,availbits,rb,&esp,gzfile);
                num=49+esp;
                break;
            case 12:
                getData(5,availbits,rb,&esp,gzfile);
                num=65+esp;
                break;
            case 13:
                getData(5,availbits,rb,&esp,gzfile);
                num=97+esp;
                break;
            case 14:
                getData(6,availbits,rb,&esp,gzfile);
                num=129+esp;
                break;
            case 15:
                getData(6,availbits,rb,&esp,gzfile);
                num=193+esp;
                break;
            case 16:
                getData(7,availbits,rb,&esp,gzfile);
                num=257+esp;
                break;
            case 17:
                getData(7,availbits,rb,&esp,gzfile);
                num=385+esp;
                break;
            case 18:
                getData(8,availbits,rb,&esp,gzfile);
                num=513+esp;
                break;
            case 19:
                getData(8,availbits,rb,&esp,gzfile);
                num=769+esp;
                break;
            case 20:
                getData(9,availbits,rb,&esp,gzfile);
                num=1025+esp;
                break;
            case 21:
                getData(9,availbits,rb,&esp,gzfile);
                num=1537+esp;
                break;
            case 22:
                getData(10,availbits,rb,&esp,gzfile);
                num=2049+esp;
                break;
            case 23:
                getData(10,availbits,rb,&esp,gzfile);
                num=3073+esp;
                break;
            case 24:
                getData(11,availbits,rb,&esp,gzfile);
                num=4097+esp;
                break;
            case 25:
                getData(11,availbits,rb,&esp,gzfile);
                num=6145+esp;
                break;
            case 26:
                getData(12,availbits,rb,&esp,gzfile);
                num=8193+esp;
                break;
            case 27:
                getData(12,availbits,rb,&esp,gzfile);
                num=12289+esp;
                break;
            case 28:
                getData(13,availbits,rb,&esp,gzfile);
                num=16385+esp;
                break;
            case 29:
                getData(13,availbits,rb,&esp,gzfile);
                num=24577+esp;
                break;
    }
    return num;

}
int comprimento(int comp,unsigned long *availbits,unsigned long *rb,FILE *gzfile){
    int num;
    unsigned int esp;
    switch(comp){
            case 257:
                num=3;
                break;
            case 258:
                num=4;
                break;
            case 259:
                num=5;
                break;
            case 260:
                num=6;
                break;
            case 261:
                num=7;
                break;
            case 262:
                num=8;
                break;
            case 263:
                num=9;
                break;
            case 264:
                num=10;
                break;
            case 265:
                getData(1,availbits,rb,&esp,gzfile);
                num=11+esp;
                break;
            case 266:
                getData(1,availbits,rb,&esp,gzfile);
                num=13+esp;
                break;
            case 267:
                getData(1,availbits,rb,&esp,gzfile);
                num=15+esp;
                break;
            case 268:
                getData(1,availbits,rb,&esp,gzfile);
                num=17+esp;
                break;
            case 269:
                getData(2,availbits,rb,&esp,gzfile);
                num=19+esp;
                break;
            case 270:
                getData(2,availbits,rb,&esp,gzfile);
                num=23+esp;
                break;
            case 271:
                getData(2,availbits,rb,&esp,gzfile);
                num=27+esp;
                break;
            case 272:
                getData(2,availbits,rb,&esp,gzfile);
                num=31+esp;
                break;
            case 273:
                getData(3,availbits,rb,&esp,gzfile);
                num=35+esp;
                break;
            case 274:
                getData(3,availbits,rb,&esp,gzfile);
                num=43+esp;
                break;
            case 275:
                getData(3,availbits,rb,&esp,gzfile);
                num=51+esp;
                break;
            case 276:
                getData(3,availbits,rb,&esp,gzfile);
                num=59+esp;
                break;
            case 277:
                getData(4,availbits,rb,&esp,gzfile);
                num=67+esp;
                break;
            case 278:
                getData(4,availbits,rb,&esp,gzfile);
                num=83+esp;
                break;
            case 279:
                getData(4,availbits,rb,&esp,gzfile);
                num=99+esp;
                break;
            case 280:
                getData(4,availbits,rb,&esp,gzfile);
                num=115+esp;
                break;
            case 281:
                getData(5,availbits,rb,&esp,gzfile);
                num=131+esp;
                break;
            case 282:
                getData(5,availbits,rb,&esp,gzfile);
                num=163+esp;
                break;
            case 283:
                getData(5,availbits,rb,&esp,gzfile);
                num=195+esp;
                break;
            case 284:
                getData(5,availbits,rb,&esp,gzfile);
                num=227+esp;
                break;
            case 285:
                num=258;
                break;
        }
        return num;
}

void criarHuffman(char *Arraycomp, char **HuffmanArray,int tam_2,HuffmanTree* hft){
    int tam=1;
    char *aux;
    aux =(char*)malloc(tam*sizeof(char));
    memset(aux,0,tam);
    for(int i=0;i<tam_2;i++){
        if(notContains(aux,Arraycomp[i],tam)){   //Verificar se o comprimento j� se encontra no aux;
                tam++;
                char *tmp;
                tmp = (char *)realloc(aux,tam*sizeof(char));
                if (tmp!=NULL)
                    aux = tmp;
                aux[tam-1]=Arraycomp[i];
        }
    }
    //Fun��o para ordenamento do vetor auxiliar que cont�m os comprimentos
    char n;
    for(int i=0;i<tam;i++){
        int j;
        for(j=0;j<tam;j++){
            if(aux[i]<aux[j]){
                n=aux[i];
                aux[i]=aux[j];
                aux[j]=n;
            }
        }
    }
    unsigned long bits=0;
    int indice=0;
    for(int i=1;i<tam;i++){
        for(int k=0;k<tam_2;k++){
            if (Arraycomp[k]==(aux[i])){
                bits2String(HuffmanArray[k],bits,aux[i]); //converter os bits para string
                bits+=1;

            }
        }
        if(i+1!=tam)
            bits =(aux[i+1]-aux[i])*(bits)*2;

    }

    short verbose=0;
    int x;
    for(int i=1;i<tam;i++){
        for(int k=0;k<tam_2;k++){
            if(aux[i]==Arraycomp[k]){
                x=addNode(hft, HuffmanArray[k], k, verbose); //adicionar � �rvore de huffman
            }
        }
    }
}
bool notContains(char *a, char b,int tam){

    for(int k=0;k<tam;k++){
        if (a[k]==b)
            return false;
        }
    return true;
}



void getData(unsigned long needBits,unsigned long *availBits,unsigned long *rb,unsigned int *nome,FILE* gzFile){
    unsigned char byte;

    while (*(availBits) < needBits)
        {
            fread(&byte, 1, 1, gzFile);
            *rb = (byte << *(availBits)) | *rb;
            *availBits += 8;
        }
    unsigned long mask = (1 << needBits) - 1;
    *nome = *rb & mask;
    *rb = *rb >> needBits;
    *availBits-=needBits;
}




//---------------------------------------------------------------
//L� o cabe�alho do ficheiro gzip: devolve erro (-1) se o formato for inv�lidodevolve, ou 0 se ok
int getHeader(FILE *gzFile, gzipHeader *gzh) //obt�m cabe�alho
{
    unsigned char byte;

    //Identica��o 1 e 2: valores fixos
    fread(&byte, 1, 1, gzFile);
    (*gzh).ID1 = byte;
    if ((*gzh).ID1 != 0x1f) return -1; //erro no cabe�alho

    fread(&byte, 1, 1, gzFile);
    (*gzh).ID2 = byte;
    if ((*gzh).ID2 != 0x8b) return -1; //erro no cabe�alho

    //M�todo de compress�o (deve ser 8 para denotar o deflate)
    fread(&byte, 1, 1, gzFile);
    (*gzh).CM = byte;
    if ((*gzh).CM != 0x08) return -1; //erro no cabe�alho

    //Flags
    fread(&byte, 1, 1, gzFile);
    unsigned char FLG = byte;

    //MTIME
    char lenMTIME = 4;
    fread(&byte, 1, 1, gzFile);
    (*gzh).MTIME = byte;
    for (int i = 1; i <= lenMTIME - 1; i++)
    {
        fread(&byte, 1, 1, gzFile);
        (*gzh).MTIME = (byte << 8) + (*gzh).MTIME;
    }

    //XFL (not processed...)
    fread(&byte, 1, 1, gzFile);
    (*gzh).XFL = byte;

    //OS (not processed...)
    fread(&byte, 1, 1, gzFile);
    (*gzh).OS = byte;

    //--- Check Flags
    (*gzh).FLG_FTEXT = (char)(FLG & 0x01);
    (*gzh).FLG_FHCRC = (char)((FLG & 0x02) >> 1);
    (*gzh).FLG_FEXTRA = (char)((FLG & 0x04) >> 2);
    (*gzh).FLG_FNAME = (char)((FLG & 0x08) >> 3);
    (*gzh).FLG_FCOMMENT = (char)((FLG & 0x10) >> 4);

    //FLG_EXTRA
    if ((*gzh).FLG_FEXTRA == 1)
    {
        //ler 2 bytes XLEN + XLEN bytes de extra field
        //1� byte: LSB, 2�: MSB
        char lenXLEN = 2;

        fread(&byte, 1, 1, gzFile);
        (*gzh).xlen = byte;
        fread(&byte, 1, 1, gzFile);
        (*gzh).xlen = (byte << 8) + (*gzh).xlen;

        (*gzh).extraField = new unsigned char[(*gzh).xlen];

        //ler extra field (deixado como est�, i.e., n�o processado...)
        for (int i = 0; i <= (*gzh).xlen - 1; i++)
        {
            fread(&byte, 1, 1, gzFile);
            (*gzh).extraField[i] = byte;
        }
    }
    else
    {
        (*gzh).xlen = 0;
        (*gzh).extraField = 0;
    }

    //FLG_FNAME: ler nome original
    if ((*gzh).FLG_FNAME == 1)
    {
        (*gzh).fName = new char[1024];
        unsigned int i = 0;
        do
        {
            fread(&byte, 1, 1, gzFile);
            if (i <= 1023)  //guarda no m�ximo 1024 caracteres no array
                (*gzh).fName[i] = byte;
            i++;
        }while(byte != 0);
        if (i > 1023)
            (*gzh).fName[1023] = 0;  //apesar de nome incompleto, garantir que o array termina em 0
    }
    else
        (*gzh).fName = 0;

    //FLG_FCOMMENT: ler coment�rio
    if ((*gzh).FLG_FCOMMENT == 1)
    {
        (*gzh).fComment = new char[1024];
        unsigned int i = 0;
        do
        {
            fread(&byte, 1, 1, gzFile);
            if (i <= 1023)  //guarda no m�ximo 1024 caracteres no array
                (*gzh).fComment[i] = byte;
            i++;
        }while(byte != 0);
        if (i > 1023)
            (*gzh).fComment[1023] = 0;  //apesar de coment�rio incompleto, garantir que o array termina em 0
    }
    else
        (*gzh).fComment = 0;


    //FLG_FHCRC (not processed...)
    if ((*gzh).FLG_FHCRC == 1)
    {
        (*gzh).HCRC = new unsigned char[2];
        fread(&byte, 1, 1, gzFile);
        (*gzh).HCRC[0] = byte;
        fread(&byte, 1, 1, gzFile);
        (*gzh).HCRC[1] = byte;
    }
    else
        (*gzh).HCRC = 0;

    return 0;
}


//---------------------------------------------------------------
//Analisa block header e v� se � huffman din�mico
int isDynamicHuffman(unsigned char rb)
{
    unsigned char BTYPE = rb & 0x03;

    if (BTYPE == 0) //--> sem compress�o
    {
        printf("Ignorando bloco: sem compacta��o!!!\n");
        return 0;
    }
    else if (BTYPE == 1)
    {
        printf("Ignorando bloco: compactado com Huffman fixo!!!\n");
        return 0;
    }
    else if (BTYPE == 3)
    {
        printf("Ignorando bloco: BTYPE = reservado!!!\n");
        return 0;
    }
    else
        return 1;
}


//---------------------------------------------------------------
//Obt�m tamanho do ficheiro original
long getOrigFileSize(FILE * gzFile)
{
    //salvaguarda posi��o actual do ficheiro
    long fp = ftell(gzFile);

    //�ltimos 4 bytes = ISIZE;
    fseek(gzFile, -4, SEEK_END);

    //determina ISIZE (s� correcto se cabe em 32 bits)
    unsigned long sz = 0;
    unsigned char byte;
    fread(&byte, 1, 1, gzFile);
    sz = byte;
    for (int i = 0; i <= 2; i++)
    {
        fread(&byte, 1, 1, gzFile);
        sz = (byte << 8*(i+1)) + sz;
    }


    //restaura file pointer
    fseek(gzFile, fp, SEEK_SET);

    return sz;
}


//---------------------------------------------------------------
void bits2String(char *strBits, unsigned long byte,int tam)
{
    char mask = 0x1;  //get LSbit

    strBits[tam] = 0;
    for (char bit, i = tam-1; i >= 0; i--)
    {
        bit = byte & mask;
        strBits[i] = bit +48; //converter valor num�rico para o caracter alfanum�rico correspondente
        byte = byte >> 1;
    }
}
