"use strict"

class BotaoMenu
{
	constructor(x, y,width,height,img)
	{
		//posição
		this.x = x;
		this.y = y;

		//tamanho Original
		this.widthOriginal=width;
		this.heightOriginal=height;
		//tamanho
		this.width=width;
		this.height=height;
		//imagem
		this.img = img;
		this.rezizable=true;
		this.audio=true;
	}
	draw(ctx)
	{
		ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
	}
	clear(ctx)
	{
		ctx.clearRect(this.x, this.y, this.width, this.height);
	}
	mouseOverBoundingBox(mouseClickPosition)
	{
		if (mouseClickPosition[0] >= this.x && mouseClickPosition[0] <= this.x + this.width && mouseClickPosition[1] >= this.y && mouseClickPosition[1] <= this.y + this.height){
			return true;
		}
		return false;
	}
	resize(){
		this.width=this.widthOriginal;
		this.height=this.heightOriginal;
		this.rezizable=true;
		this.audio=true;
	}
	aumentaTamanho(){
		if(this.rezizable){
			this.width*=1.05;
			this.height*=1.05;
			this.rezizable=false;
		}
	}
	efeitoSom(ArrayMusicas){
		if(this.audio){
			ArrayMusicas[1].play();
			this.audio=false;
		}
	}
}
