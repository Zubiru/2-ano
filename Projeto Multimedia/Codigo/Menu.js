"use strict"

class Menu
{
  constructor(ctx)
	{
    this.ctx=ctx;
    this.spArray=new Array();
	}
  desenha(numMenu,ArrayNiveis)
  {
    var i=0;
    for(i=0;i<this.spArray.length;i++){
      this.spArray[i].draw(this.ctx);
    }
    if(numMenu==2){
      var y=220;
      var x=310;
      for(i=0;i<ArrayNiveis.length;i++){
        var melhorPontuacao=ArrayNiveis[i].melhorPontuacao;
      	this.ctx.font="30px Verdana";
        var nivel=i+1;
      	this.ctx.fillText("Level "+nivel+"-> "+melhorPontuacao,x,y);
        y+=40;
      }

    }
  }
  apaga(){
    this.ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
  animacaoBotoes(mousePosition,ArrayMusicas){
    var i=0
    for(i=0;i<this.spArray.length;i++){
      if(this.spArray[i].mouseOverBoundingBox(mousePosition)){
        if(this.spArray[i].img.id.slice(0,5)!="fundo" && this.spArray[i].img.id!="titulo"){
          this.spArray[i].aumentaTamanho();
          this.spArray[i].efeitoSom(ArrayMusicas);
        }
      }
      else{
        this.spArray[i].resize();
      }
    }
  }
  verificaBotaoCarregado(mousePosition,ArrayMusicas,esquema){
    var i=1;
    for(i;i<this.spArray.length;i++){
      if(this.spArray[i].mouseOverBoundingBox(mousePosition)){
        this.ReacaoBotoesOpcoes(i,ArrayMusicas,esquema);
      }
    }

  }
  ResolveMouseClick(numMenu,reqID,ctx,ArrayMusicas,ArrayMenus,mouseClickPosition,esquema){
    if(numMenu[0]==0){
          if(ArrayMenus[numMenu].spArray[0].mouseOverBoundingBox(mouseClickPosition)){
            numMenu[0]=4;
          }
          else if(ArrayMenus[numMenu].spArray[1].mouseOverBoundingBox(mouseClickPosition)){
            numMenu[0]=1;
          }
          else if(ArrayMenus[numMenu].spArray[2].mouseOverBoundingBox(mouseClickPosition)){
            numMenu[0]=2;

          }
          else if(ArrayMenus[numMenu].spArray[3].mouseOverBoundingBox(mouseClickPosition)){
            numMenu[0]= 3;

          }
          else if(ArrayMenus[numMenu].spArray[4].mouseOverBoundingBox(mouseClickPosition)){
            window.close();
            numMenu[0]= 0;

          }
    }
    else if(numMenu[0]==1){
        if(ArrayMenus[numMenu].spArray[0].mouseOverBoundingBox(mouseClickPosition)){//Back
          numMenu[0]= 0;
        }
        ArrayMenus[numMenu[0]].verificaBotaoCarregado(mouseClickPosition,ArrayMusicas,esquema);
    }
    else if(numMenu[0]==2){
        if(ArrayMenus[numMenu[0]].spArray[0].mouseOverBoundingBox(mouseClickPosition)){
          numMenu[0]= 0;
        }

    }
    else if(numMenu[0]==3){
        if(ArrayMenus[numMenu].spArray[0].mouseOverBoundingBox(mouseClickPosition)){
          numMenu[0]= 0;
        }
    }
    else if(numMenu[0]==4){
        if(ArrayMenus[numMenu].spArray[0].mouseOverBoundingBox(mouseClickPosition)){
        numMenu[0]= 0;
        }
        else if(ArrayMenus[numMenu].spArray[2].mouseOverBoundingBox(mouseClickPosition)){
          this.ReacaoNivelEscolhido(reqID,ctx,ArrayMusicas,1);
        }
        else if(ArrayMenus[numMenu].spArray[3].mouseOverBoundingBox(mouseClickPosition)){
          this.ReacaoNivelEscolhido(reqID,ctx,ArrayMusicas,2);
        }
        else if(ArrayMenus[numMenu].spArray[4].mouseOverBoundingBox(mouseClickPosition)){
          this.ReacaoNivelEscolhido(reqID,ctx,ArrayMusicas,3);
        }

    }
  }
  ReacaoBotoesOpcoes(indice,ArrayMusicas,esquema){
    if(this.spArray[indice].img.id=="aumentaVol"){
      var i;
      for(i=0;i<ArrayMusicas.length;i++){
        if(ArrayMusicas[i].volume<=0.9){
          ArrayMusicas[i].volume+=0.1;
        }
      }
    }
    else if(this.spArray[indice].img.id=="diminuiVol"){
      var i;
      for(i=0;i<ArrayMusicas.length;i++){
        if(ArrayMusicas[i].volume>=0.1){
          ArrayMusicas[i].volume-=0.1;
        }
      }

    }
    else if(this.spArray[indice].img.id=="somOff"){
      var i;
      for(i=0;i<ArrayMusicas.length;i++){
        ArrayMusicas[i].muted=true
      }
    }
    else if(this.spArray[indice].img.id=="somOn"){
      var i;
      for(i=0;i<ArrayMusicas.length;i++){
        ArrayMusicas[i].muted=false
      }

    }
    else if(this.spArray[indice].img.id=="seleciona"){
      if(indice==8){
        esquema[0]=2;
        var aux1= this.spArray[8];
        var aux2= this.spArray[9];
        this.spArray[8]=this.spArray[7];
        this.spArray[9]=this.spArray[6];
        this.spArray[7]=aux1;
        this.spArray[6]=aux2;
      }
      else{
        esquema[0]=1;
        var aux1= this.spArray[8];
        var aux2= this.spArray[9];
        this.spArray[8]=this.spArray[7];
        this.spArray[9]=this.spArray[6];
        this.spArray[7]=aux1;
        this.spArray[6]=aux2;
      }
    }
  }

  ReacaoNivelEscolhido(reqID,ctx,ArrayMusicas,indice){
    window.cancelAnimationFrame(reqID);
    ArrayMusicas[0].pause();
    ArrayMusicas[0].currentTime=0;
    var ev2 = new CustomEvent("nivelEscolhido",{
    detail: {
        nivel:indice
    }
    });
    ctx.canvas.dispatchEvent(ev2);
  }


}
