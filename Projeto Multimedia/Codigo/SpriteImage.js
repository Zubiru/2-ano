"use strict"

class SpriteImage
{
	constructor(x, y,width,height,img)
	{
		//posição
		this.x = x;
		this.y = y;
		//tamanho
		this.width=width;
		this.height=height;
		//imagem
		this.img = img;
		this.imageData;
	}
	getDadosImagem(){
		var canvas_aux = document.createElement("canvas");
		canvas_aux.id=canvas_aux;
		canvas_aux.width=800;
		canvas_aux.height=600;
		canvas_aux.margin= "auto";
		var ctx2 = canvas_aux.getContext("2d");
		ctx2.drawImage(this.img,0,0,this.width, this.height);
		this.imageData = ctx2.getImageData(0,0,this.width,this.height);
	}
	draw(ctx)
	{
		ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
	}
}
