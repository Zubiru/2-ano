"use strict"

class Nivel
{
	constructor(ctx,data,coordenadasSemColisao,coordenadasComColisao,x,y,ArrayMoveis,numeroLinhas,numeroColunas,ArrayInimigos)
	{
		this.fundo=new Image();
    this.ctx=ctx;

		this.numeroLinhas=numeroLinhas;
		this.numeroColunas=numeroColunas;

		this.ArrayBlocos=new Array();
		this.ArrayData=data;
		this.ArrayObjetosColisao=new Array(); //Array com todos os objetos com  os quais se pode colidir mesmo que nao estejam no nivel
		this.ArrayObjetosSemColisao=new Array();
		this.ArrayBlocosMoveis=ArrayMoveis;
		this.ArrayInimigos=ArrayInimigos;
		this.ArrayPaused=new Array();
		this.ArrayLevelCompleted=new Array();


		this.coordenadasSemColisao=coordenadasSemColisao;
		this.coordenadasComColisao=coordenadasComColisao;//1ª-Img 2ª-CoordenadaX 3º-CoordenadaY 4º-Se ja foi apanhado

		this.xInicialPersonagem=x;
		this.yInicialPersonagem=y;

		this.melhorPontuacao=0;

		this.tamanhoBloco=40;
	}
  desenha(time){
		this.ctx.clearRect(0, 0,this.ctx.canvas.width,this.ctx.canvas.height);
		this.ctx.drawImage(this.fundo,0,0);
		var x=0;
		var y=0;
		var i;
		for(i=0;i<this.ArrayData.length;i++){
			if(this.ArrayData[i]!=0){
				this.ctx.drawImage(this.ArrayBlocos[this.ArrayData[i]-1].img,x,y);
			}
			x+=this.tamanhoBloco;
			if(x>=this.ctx.canvas.width){
				x=0;
				y+=this.tamanhoBloco;
			}
		}
		for(i=0;i<this.coordenadasSemColisao.length;i+=3){
				this.ctx.drawImage(this.ArrayObjetosSemColisao[this.coordenadasSemColisao[i]].img,this.coordenadasSemColisao[i+1],this.coordenadasSemColisao[i+2]);
		}

		for(i=0;i<this.coordenadasComColisao.length;i+=4){
				if(this.coordenadasComColisao[i+3]){
					this.ctx.drawImage(this.ArrayObjetosColisao[this.coordenadasComColisao[i]].img,this.coordenadasComColisao[i+1],this.coordenadasComColisao[i+2]);
				}
		}
		for(i=0;i<this.ArrayBlocosMoveis.length;i++){
			this.ArrayBlocosMoveis[i].desenhaBlocos(this.ctx,this.ArrayBlocos);
			this.ArrayBlocosMoveis[i].moveBloco();
		}
		for(i=0;i<this.ArrayInimigos.length;i++){
				this.ArrayInimigos[i].animacaoCorrer(time,this.ctx);
		}
		this.ArrayPaused[1].draw(this.ctx);

	}

	atribuiCoordenadas(){
		var i;
		for(i=0;i<this.coordenadasComColisao.length;i+=4){
				this.ArrayObjetosColisao[this.coordenadasComColisao[i]].x=this.coordenadasComColisao[i+1];
				this.ArrayObjetosColisao[this.coordenadasComColisao[i]].y=this.coordenadasComColisao[i+2];
		}
	}
	pontuacao(tempo){
		var conta=0;
		var i=0;
		for(i=0;i<this.coordenadasComColisao.length;i+=4){
			if((this.coordenadasComColisao[i]!=0 || this.coordenadasComColisao[i]!=1 || this.coordenadasComColisao[i]!=6 ) && this.coordenadasComColisao[i+3]==false){
				conta+=3000;
			}
		}
		for(i=0;i<this.ArrayInimigos.length;i++){
			if(!this.ArrayInimigos[i].vivo){
				conta+=100;
			}
		}
		var x = 10000;
		tempo = tempo/1000; //milisegundos para segundos
		var p = Math.pow(0.9,tempo);
		conta+=Math.round((p*x));
		return conta;
	}
	verificaColisaoVertical(tipo,personagem,tileY){
		if(tipo ==2 || tipo==13 || tipo==14 || tipo==15 || tipo==1 || tipo==3 || tipo==5 || tipo==4){ //Colisao topo
			if(personagem.velocidadeY>0){
				var limite=tileY*this.tamanhoBloco;
				if(personagem.y+personagem.height>limite && personagem.yAnterior<=personagem.y){
					personagem.velocidadeY=0;
					personagem.y=limite-personagem.height;
					personagem.yAnterior=personagem.y;
					personagem.jumping=false;
					return;
				}
			}
		}
		else if(tipo==9|| tipo ==12 || tipo==16){ //Colisao por baixo
			if(personagem.velocidadeY<0){
				var limite=(tileY+1)*this.tamanhoBloco;
				if(personagem.y+personagem.height<limite){
					personagem.velocidadeY=0;
					personagem.yAnterior=personagem.y;
					personagem.y=limite-personagem.height*0.5;
					return;
				}
			}
		}
		else if(tipo==17 || tipo== 18){
			if(personagem.velocidadeY>0){
				var limite=tileY*this.tamanhoBloco;
				if(personagem.y+personagem.height>limite && personagem.yAnterior<personagem.y){
					personagem.morreu=true;
				}
			}
		}
	}
	verificaColisaoHorizontal(tipo,personagem,tileX){
		if(tipo==1 || tipo==4 || tipo==8 || tipo==12){ //Colisao Esquerda
			if(personagem.velocidadeX>0){
				var lateralEsquerda=tileX*this.tamanhoBloco;
				if(personagem.x+personagem.width*0.5>lateralEsquerda){ //personagem.xAnterior+personagem.width*0.5<=lateralEsquerda
					personagem.velocidadeX=0;
					personagem.x=lateralEsquerda-personagem.width*0.5;
					personagem.xAnterior=personagem.x;
					return;
				}
			}
		}
		if(tipo==3 || tipo==6 || tipo==10 || tipo==16|| tipo==1){// Colisao Direita
			if(personagem.velocidadeX<0){
				var lateralDireita=(tileX+1)*this.tamanhoBloco;
				if(personagem.x+personagem.width*0.5<lateralDireita ){ //personagem.xAnterior>=personagem.x
					personagem.velocidadeX=0;
					personagem.x=lateralDireita-personagem.width*0.5;
					personagem.xAnterior=personagem.x;
					return;
				}
			}
		}
		else if(tipo==5){
			if(personagem.velocidadeX<0){ //Direita
				var lateralDireita=(tileX+1)*this.tamanhoBloco;
				if(personagem.x+personagem.width*0.5<lateralDireita){
					personagem.velocidadeX=0;
					personagem.x=lateralDireita-personagem.width*0.5;
					personagem.xAnterior=personagem.x;
					return;
				}
			}
			else if(personagem.velocidadeX>0){
				var lateralEsquerda=tileX*this.tamanhoBloco;
				if(personagem.x+personagem.width*0.5>lateralEsquerda){
					personagem.velocidadeX=0;
					personagem.x=lateralEsquerda-personagem.width*0.5;
					personagem.xAnterior=personagem.x;
					return;
				}
			}
			return;
		}

	}
	resolveColisao(objeto,personagem,ArrayMusicas){
		if(objeto==0){
			personagem.velocidadeY=0;
			personagem.velocidadeY=-20;
			personagem.jumping=true;
			ArrayMusicas[4].play();
		}
		else if(objeto==1){
			personagem.velocidadeY=0;
			personagem.velocidadeY=-30;
			personagem.jumping=true;
			ArrayMusicas[4].play();
		}
		else if(objeto==6){
			return 0;

		}
		else{
			var i;
			for(i=0;i<this.coordenadasComColisao.length;i+=4){
				if(this.coordenadasComColisao[i]==objeto){
					ArrayMusicas[3].play();
					this.coordenadasComColisao[i+3]=false;
				}
			}
		}
	}
	resetNivel(){
		var i;
		for(i=3;i<this.coordenadasComColisao.length;i+=4){
				this.coordenadasComColisao[i]=true;
		}
		for(i=1;i<this.ArrayLevelCompleted.length;i++){
				this.ArrayLevelCompleted[i].resize();
		}
		for(i=1;i<this.ArrayPaused.length;i++){
				this.ArrayPaused[i].resize();
		}
		for(i=0;i<this.ArrayInimigos.length;i++){
				this.ArrayInimigos[i].resetInimigo();
		}
	}
	VerificaColisaoBlocosMoveis(personagem,tileXPersonagem,tileYPersonagem){
		var i;
		var tileXBloco;
		var tileYBloco;
		for(i=0;i<this.ArrayBlocosMoveis.length;i++){
			tileXBloco=Math.floor((this.ArrayBlocosMoveis[i].x)/40);
			tileYBloco=Math.floor((this.ArrayBlocosMoveis[i].y)/40);
			if(this.ArrayBlocosMoveis[i].direcao){
				if(tileXPersonagem==tileXBloco && tileYBloco>=tileYPersonagem-1 && tileYBloco<tileYPersonagem+1){ //Devido a um arredondamento nao se consegue colocar ==
					this.ArrayBlocosMoveis[i].colisaoPersonagem(personagem);
				}
			}
			else{
				if(tileYPersonagem==tileYBloco && tileXBloco>=tileXPersonagem-1 && tileXBloco<tileXPersonagem+1){ //Devido a um arredondamento nao se consegue colocar ==
					this.ArrayBlocosMoveis[i].colisaoPersonagem(personagem);
				}

			}
	}
}
	DesenhaFimNivel(){
		var i;
		for(i=0;i<this.ArrayLevelCompleted.length;i++){
				this.ArrayLevelCompleted[i].draw(this.ctx);
		}
	}
	DesenhaPausa(){
		var i;
		for(i=0;i<this.ArrayPaused.length;i++){
				this.ArrayPaused[i].draw(this.ctx);
		}
	}
	animacaoBotoes(mousePosition,ArrayMusicas,Frame){
    var i=0
		if(Frame==0){ //Animacao Nivel
			if(this.ArrayPaused[1].mouseOverBoundingBox(mousePosition)){
          this.ArrayPaused[1].aumentaTamanho();
          this.ArrayPaused[1].efeitoSom(ArrayMusicas);
      }
      else{
        this.ArrayPaused[1].resize();
      }
		}
		else if(Frame==2){ //Animacao Fim Nivel
			for(i=1;i<this.ArrayLevelCompleted.length;i++){
	      if(this.ArrayLevelCompleted[i].mouseOverBoundingBox(mousePosition)){
	          this.ArrayLevelCompleted[i].aumentaTamanho();
	          this.ArrayLevelCompleted[i].efeitoSom(ArrayMusicas);
	      }
	      else{
	        this.ArrayLevelCompleted[i].resize();
	      }
	    }

		}
		else if(Frame==1){ //Animacao Pausa
			for(i=2;i<this.ArrayPaused.length;i++){
	      if(this.ArrayPaused[i].mouseOverBoundingBox(mousePosition)){
	          this.ArrayPaused[i].aumentaTamanho();
	          this.ArrayPaused[i].efeitoSom(ArrayMusicas);
	      }
	      else{
	        this.ArrayPaused[i].resize();
	      }
	    }

		}

  }
	atualizaPontuacao(pontuacao){
		if(pontuacao>this.melhorPontuacao){
			this.melhorPontuacao=pontuacao;
		}
	}


}
