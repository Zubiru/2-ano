"use strict"

class BlocoMovel{
  constructor(tipo,xInicial,yInicial,direcao,distancia,orientacao,velocidade){
    this.tipo = tipo;
    this.xInicial=xInicial;
    this.yInicial=yInicial;
    if(!orientacao && !direcao){
      this.limiteInferiorX=xInicial-distancia;
      this.limiteSuperiorX=xInicial;
    }
    else if(orientacao && !direcao){
      this.limiteSuperiorX=xInicial+distancia;
      this.limiteInferiorX=xInicial;
    }
    else if(!orientacao && direcao){
      this.limiteInferiorY=yInicial-distancia;
      this.limiteSuperiorY=yInicial;
    }
    else if(orientacao && direcao){
      this.limiteSuperiorY=yInicial+distancia;
      this.limiteInferiorY=yInicial;
    }
    this.x=xInicial;
    this.y=yInicial;
    this.direcao=direcao;
    this.distancia=distancia;
    this.orientacao=orientacao;
    this.velocidade=velocidade;
  }
  moveBloco(){
    if(this.direcao){//TRUE = VERTICAL FALSE=HORIZONTAL
        if(this.orientacao){ //TRUE = DESCE FALSE = SOBE
            this.y+=this.velocidade;
          if(this.y>this.limiteSuperiorY){
            this.orientacao=false;
          }
        }
        else{
          this.y-=this.velocidade;
          if(this.y<this.limiteInferiorY){
            this.orientacao=true;
          }
        }
    }
    else{
        if(this.orientacao){ //TRUE = anda para a direita FALSE = anda para a esquerda
            this.x+=this.velocidade;
          if(this.x>=this.limiteSuperiorX){
            this.orientacao=false;
          }
        }
        else{
          this.x-=this.velocidade;
          if(this.x<=this.limiteInferiorX){
            this.orientacao=true;
          }
        }
    }

  }

  desenhaBlocos(ctx,ArrayBlocos){
    ctx.drawImage(ArrayBlocos[this.tipo-1].img,this.x,this.y);
  }

  colisaoPersonagem(personagem){
    if(this.direcao){
      if(personagem.velocidadeY>0){
          if(personagem.y+personagem.height>=this.y){
            personagem.velocidadeY=0;
            personagem.y=this.y-personagem.height;
            personagem.yAnterior=personagem.y;
            personagem.jumping=false;
            return
          }
          else if(this.orientacao){
            personagem.y+=this.velocidade;

          }
          else{
            personagem.y-=this.velocidade;
          }
    }
  }
  else{
    if(personagem.velocidadeY>0){
        if(personagem.y+personagem.height>=this.y){
          personagem.velocidadeY=0;
          personagem.y=this.y-personagem.height;
          personagem.yAnterior=personagem.y;
          personagem.jumping=false;
        }
        if(this.orientacao){
          personagem.x+=this.velocidade;

        }
        else{
          personagem.x-=this.velocidade;
        }
      }

  }



}

}
