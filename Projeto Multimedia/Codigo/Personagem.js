"use strict"

class Personagem
{
	constructor(x,y)
	{
		//posição e movimento
		this.xAnterior = x;
		this.yAnterior = y;

		this.x = x;
		this.y = y;

		this.width=0;
		this.height=0;

		this.velocidadeX=0;
		this.velocidadeY=0;

    this.spArrayParadoDireita=new Array();
    this.spArrayCorrerDireita=new Array();
    this.spArraySaltarDireita=new Array();
    this.spArrayMorrerDireita=new Array();
		this.spArrayDeslizarDireita=new Array();

		this.spArrayParadoEsquerda=new Array();
    this.spArrayCorrerEsquerda=new Array();
    this.spArraySaltarEsquerda=new Array();
    this.spArrayMorrerEsquerda=new Array();
		this.spArrayDeslizarEsquerda=new Array();

		this.indiceParado=0;
		this.indiceCorrer=0;
		this.indiceSaltar=0;
		this.indiceMorrer=0;
		this.indiceDeslizar=0;


		//Saber ultima imagem utilizada na animação
		this.parado=true;
		this.correr=false;
		this.saltar=false;
		this.deslizar=false;

		this.orientacao=true // true == Direita, false== Esquerda
		this.jumping=false; // variavel para verificar se a personagem está no ar

		this.tempoAnimacao=0;
		this.morreu=false;
	}

	animacaoParado(time,ctx){
		if(time-this.tempoAnimacao>100){
			if(this.indiceParado==9){
					this.indiceParado=0;
				}
				else{
					this.indiceParado++;
				}
				this.tempoAnimacao=time;
		}
		this.deslizar=false;
		if(this.orientacao){
			this.spArrayParadoDireita[this.indiceParado].x=this.x;
			this.spArrayParadoDireita[this.indiceParado].y=this.y;
			this.width=this.spArrayParadoDireita[this.indiceParado].width;
			this.height=this.spArrayParadoDireita[this.indiceParado].height;
			this.spArrayParadoDireita[this.indiceParado].draw(ctx);
		}
		else{
			this.spArrayParadoEsquerda[this.indiceParado].x=this.x
			this.spArrayParadoEsquerda[this.indiceParado].y=this.y
			this.width=this.spArrayParadoEsquerda[this.indiceParado].width;
			this.height=this.spArrayParadoEsquerda[this.indiceParado].height;
			this.spArrayParadoEsquerda[this.indiceParado].draw(ctx);

		}
		this.parado=true;
		this.correr=false;
		this.saltar=false;
		this.deslizar=false;
	}
	animacaoCorrer(time,ctx){
		if(time-this.tempoAnimacao>60){
			if(this.indiceCorrer==9){
				this.indiceCorrer=0;
			}
			else{
				this.indiceCorrer++;
			}
			this.tempoAnimacao=time;
		}
		if(this.orientacao){
			this.spArrayCorrerDireita[this.indiceCorrer].x=this.x
			this.spArrayCorrerDireita[this.indiceCorrer].y=this.y
			this.width=this.spArrayCorrerDireita[this.indiceCorrer].width;
			this.height=this.spArrayCorrerDireita[this.indiceCorrer].height;
			this.spArrayCorrerDireita[this.indiceCorrer].draw(ctx);
		}
		else{
			this.spArrayCorrerEsquerda[this.indiceCorrer].x=this.x
			this.spArrayCorrerEsquerda[this.indiceCorrer].y=this.y
			this.width=this.spArrayCorrerEsquerda[this.indiceCorrer].width;
			this.height=this.spArrayCorrerEsquerda[this.indiceCorrer].height;
			this.spArrayCorrerEsquerda[this.indiceCorrer].draw(ctx);
		}
		this.parado=false;
		this.correr=true;
		this.saltar=false;
		this.deslizar=false;
	}

	animacaoSaltar(time,ctx){
		if(time-this.tempoAnimacao>60){
			if(this.indiceSaltar==9){
				this.indiceSaltar=0;
			}
			else{
				this.indiceSaltar++;
			}
			this.tempoAnimacao=time;
		}
		if(this.orientacao){
			this.spArraySaltarDireita[this.indiceSaltar].x=this.x
			this.spArraySaltarDireita[this.indiceSaltar].y=this.y
			this.width=this.spArraySaltarDireita[this.indiceSaltar].width;
			this.height=this.spArraySaltarDireita[this.indiceSaltar].height;
			this.spArraySaltarDireita[this.indiceSaltar].draw(ctx);
		}
		else{
			this.spArraySaltarEsquerda[this.indiceSaltar].x=this.x
			this.spArraySaltarEsquerda[this.indiceSaltar].y=this.y
			this.width=this.spArraySaltarEsquerda[this.indiceSaltar].width;
			this.height=this.spArraySaltarEsquerda[this.indiceSaltar].height;
			this.spArraySaltarEsquerda[this.indiceSaltar].draw(ctx);
		}
		this.parado=false;
		this.correr=false;
		this.saltar=true;
		this.deslizar=false;
		this.jumping=true;
	}
	animacaoMorrer(time,ctx){
		if(time-this.tempoAnimacao>60){
			this.indiceMorrer++;
			this.tempoAnimacao=time;
		}
		if(this.orientacao){
			this.spArrayMorrerDireita[this.indiceMorrer].x=this.x
			this.spArrayMorrerDireita[this.indiceMorrer].y=this.y
			this.width=this.spArrayMorrerDireita[this.indiceMorrer].width;
			this.height=this.spArrayMorrerDireita[this.indiceMorrer].height;
			this.spArrayMorrerDireita[this.indiceMorrer].draw(ctx);
		}
		else{
			this.spArrayMorrerEsquerda[this.indiceMorrer].x=this.x
			this.spArrayMorrerEsquerda[this.indiceMorrer].y=this.y
			this.width=this.spArrayMorrerEsquerda[this.indiceMorrer].width;
			this.height=this.spArrayMorrerEsquerda[this.indiceMorrer].height;
			this.spArrayMorrerEsquerda[this.indiceMorrer].draw(ctx);
		}

	}
	animacaoDeslizar(time,ctx){
		if(time-this.tempoAnimacao>60){
			if(this.indiceDeslizar==9){
				this.indiceDeslizar=0;
			}
			else{
				this.indiceDeslizar++;
			}
			this.tempoAnimacao=time;
		}
		if(this.orientacao){
			this.spArrayDeslizarDireita[this.indiceDeslizar].x=this.x
			this.spArrayDeslizarDireita[this.indiceDeslizar].y=this.y
			this.width=this.spArrayDeslizarDireita[this.indiceDeslizar].width;
			this.height=this.spArrayDeslizarDireita[this.indiceDeslizar].height;
			this.spArrayDeslizarDireita[this.indiceDeslizar].draw(ctx);
		}
		else{
			this.spArrayDeslizarEsquerda[this.indiceDeslizar].x=this.x
			this.spArrayDeslizarEsquerda[this.indiceDeslizar].y=this.y
			this.width=this.spArrayDeslizarEsquerda[this.indiceDeslizar].width;
			this.height=this.spArrayDeslizarEsquerda[this.indiceDeslizar].height;
			this.spArrayDeslizarEsquerda[this.indiceDeslizar].draw(ctx);
		}
		this.parado=false;
		this.correr=false;
		this.saltar=false;
		this.deslizar=true;
	}
	resetPersonagem(x,y){
		this.xAnterior = x;
		this.yAnterior = y;

		this.x = x;
		this.y = y;

		this.width=0;
		this.height=0;

		this.velocidadeX=0;
		this.velocidadeY=0;

		this.indiceParado=0;
		this.indiceCorrer=0;
		this.indiceSaltar=0;
		this.indiceMorrer=0;
		this.indiceDeslizar=0;

		this.parado=true;
		this.correr=false;
		this.saltar=false;
		this.deslizar=false;

		this.orientacao=true // true == Direita, false== Esquerda
		this.jumping=false; // variavel para verificar se a personagem está no ar

		this.tempoAnimacao=0;
		this.morreu=false;

		var i;
		for(i=0;i<this.spArrayParadoDireita;i++){
			this.spArrayParadoDireita[i].x=this.x;
			this.spArrayParadoDireita[i].y=this.y;
		}
		for(i=0;i<this.spArrayCorrerDireita;i++){
			this.spArrayCorrerDireita[i].x=this.x;
			this.spArrayCorrerDireita[i].y=this.y;
		}
		for(i=0;i<this.spArraySaltarDireita;i++){
			this.spArraySaltarDireita[i].x=this.x;
			this.spArraySaltarDireita[i].y=this.y;
		}
		for(i=0;i<this.spArrayMorrerDireita;i++){
			this.spArrayMorrerDireita[i].x=this.x;
			this.spArrayMorrerDireita[i].y=this.y;
		}
		for(i=0;i<this.spArrayDeslizarDireita;i++){
			this.spArrayDeslizarDireita[i].x=this.x;
			this.spArrayDeslizarDireita[i].y=this.y;
		}
		for(i=0;i<this.spArrayParadoEsquerda;i++){
			this.spArrayParadoEsquerda[i]=this.x;
			this.spArrayParadoEsquerda[i]=this.y;
		}
		for(i=0;i<this.spArrayCorrerEsquerda;i++){
			this.spArrayCorrerEsquerda[i].x=this.x;
			this.spArrayCorrerEsquerda[i].y=this.y;
		}
		for(i=0;i<this.spArraySaltarEsquerda;i++){
			this.spArraySaltarEsquerda[i].x=this.x;
			this.spArraySaltarEsquerda[i].y=this.y;
		}
		for(i=0;i<this.spArrayMorreuEsquerda;i++){
			this.spArrayMorrerEsquerda[i].x=this.x;
			this.spArrayMorrerEsquerda[i].y=this.y;
		}
		for(i=0;i<this.spArrayDeslizarEsquerda;i++){
			this.spArrayDeslizarEsquerda[i].x=this.x;
			this.spArrayDeslizarEsquerda[i].y=this.y;
		}
	}

	posicaoIncialNivel(nivelEscolhido){
		this.xAnterior = nivelEscolhido.xInicialPersonagem;
		this.yAnterior = nivelEscolhido.yInicialPersonagem;

		this.x =nivelEscolhido.xInicialPersonagem;
		this.y =nivelEscolhido.yInicialPersonagem;
	}

	verificarColisoes(ArrayObjetosColisao,coordenadasComColisao,ArrayInimigos,ctx){
		var i=0;
		if(this.parado){ //Caso a ultima imagem utilizada pertença ao arrayParado
				if(this.orientacao){ // Saber se está virado para a direita ou esquerda
						var aX_1=this.x
						var aY_1=this.y
						var aX_2=this.x+this.spArrayParadoDireita[this.indiceParado].width;
						var aY_2=this.y+this.spArrayParadoDireita[this.indiceParado].height;
						//ColisaoComInimigos
						for(i=0;i<ArrayInimigos.length;i++){
								if(!ArrayInimigos[i].vivo){
									continue;
								}
								var iX_1=ArrayInimigos[i].x;
								var iY_1=ArrayInimigos[i].y;
								var iX_2=iX_1+ArrayInimigos[i].width;
								var iY_2=iY_1+ArrayInimigos[i].height;
								if(aX_1 > iX_2 || iX_1 > aX_2){
									continue;
								}
								if(aY_1 > iY_2 || iY_1 > aY_2){
									continue;
								}
								if(this.verificaImageData(ArrayInimigos[i].getSprite(),ArrayInimigos[i].x,ArrayInimigos[i].y,this.spArrayParadoDireita[this.indiceParado],ctx)){
									this.morreu=true;
									return;
								}
						}
						//Colisao Com Objetos
						for(i=0;i<coordenadasComColisao.length;i+=4){
							if(coordenadasComColisao[i+3]){
								var bX_1=coordenadasComColisao[i+1];
								var bY_1=coordenadasComColisao[i+2];
								var bX_2=coordenadasComColisao[i+1] + ArrayObjetosColisao[coordenadasComColisao[i]].width;
								var bY_2=coordenadasComColisao[i+2] + ArrayObjetosColisao[coordenadasComColisao[i]].height;
								if(aX_1 > bX_2 || bX_1 > aX_2){
									continue;
								}
								if(aY_1 > bY_2 || bY_1 > aY_2){
									continue;
								}
								if(this.verificaImageData(ArrayObjetosColisao[coordenadasComColisao[i]],coordenadasComColisao[i+1],coordenadasComColisao[i+2],this.spArrayParadoDireita[this.indiceParado],ctx)){
									return coordenadasComColisao[i];
								}
							}
						}
				}
				else{
					var aX_1=this.x
					var aY_1=this.y
					var aX_2=this.x+this.spArrayParadoEsquerda[this.indiceParado].width;
					var aY_2=this.y+this.spArrayParadoEsquerda[this.indiceParado].height;
					//ColisaoComInimigos
					for(i=0;i<ArrayInimigos.length;i++){
							if(!ArrayInimigos[i].vivo){
								continue;
							}
							var iX_1=ArrayInimigos[i].x;
							var iY_1=ArrayInimigos[i].y;
							var iX_2=iX_1+ArrayInimigos[i].width;
							var iY_2=iY_1+ArrayInimigos[i].height;
							if(aX_1 > iX_2 || iX_1 > aX_2){
								continue;
							}
							if(aY_1 > iY_2 || iY_1 > aY_2){
								continue;
							}
							if(this.verificaImageData(ArrayInimigos[i].getSprite(),ArrayInimigos[i].x,ArrayInimigos[i].y,this.spArrayParadoEsquerda[this.indiceParado],ctx)){
								this.morreu=true;
								return;
							}
					}
					for(i=0;i<coordenadasComColisao.length;i+=4){
						if(coordenadasComColisao[i+3]){
							var bX_1=coordenadasComColisao[i+1];
							var bY_1=coordenadasComColisao[i+2];
							var bX_2=coordenadasComColisao[i+1] + ArrayObjetosColisao[coordenadasComColisao[i]].width;
							var bY_2=coordenadasComColisao[i+2] + ArrayObjetosColisao[coordenadasComColisao[i]].height;
							if(aX_1 > bX_2 || bX_1 > aX_2){
								continue;
							}
							if(aY_1 > bY_2 || bY_1 > aY_2){
								continue;
							}
							if(this.verificaImageData(ArrayObjetosColisao[coordenadasComColisao[i]],coordenadasComColisao[i+1],coordenadasComColisao[i+2],this.spArrayParadoEsquerda[this.indiceParado],ctx)){
								return coordenadasComColisao[i];
							}
						}
					}
				}
		}
		else if(this.correr){
				if(this.orientacao){
						var aX_1=this.x
						var aY_1=this.y
						var aX_2=this.x+this.spArrayCorrerDireita[this.indiceCorrer].width;
						var aY_2=this.y+this.spArrayCorrerDireita[this.indiceCorrer].height;

						//ColisaoComInimigos
						for(i=0;i<ArrayInimigos.length;i++){
								if(!ArrayInimigos[i].vivo){
									continue;
								}
								var iX_1=ArrayInimigos[i].x;
								var iY_1=ArrayInimigos[i].y;
								var iX_2=iX_1+ArrayInimigos[i].width;
								var iY_2=iY_1+ArrayInimigos[i].height;
								if(aX_1 > iX_2 || iX_1 > aX_2){
									continue;
								}
								if(aY_1 > iY_2 || iY_1 > aY_2){
									continue;
								}
								if(this.verificaImageData(ArrayInimigos[i].getSprite(),ArrayInimigos[i].x,ArrayInimigos[i].y,this.spArrayCorrerDireita[this.indiceCorrer],ctx)){
									this.morreu=true;
									return;
								}
						}
						for(i=0;i<coordenadasComColisao.length;i+=4){
							if(coordenadasComColisao[i+3]){
								var bX_1=coordenadasComColisao[i+1];
								var bY_1=coordenadasComColisao[i+2];
								var bX_2=coordenadasComColisao[i+1] + ArrayObjetosColisao[coordenadasComColisao[i]].width;
								var bY_2=coordenadasComColisao[i+2]+ ArrayObjetosColisao[coordenadasComColisao[i]].height;
								if(aX_1 > bX_2 || bX_1 > aX_2){
									continue;
								}
								if(aY_1 > bY_2 || bY_1 > aY_2){
									continue;
								}
								if(this.verificaImageData(ArrayObjetosColisao[coordenadasComColisao[i]],coordenadasComColisao[i+1],coordenadasComColisao[i+2],this.spArrayCorrerDireita[this.indiceCorrer],ctx)){
									return coordenadasComColisao[i];
								}
							}
						}
				}
				else{
					var aX_1=this.x
					var aY_1=this.y
					var aX_2=this.x+this.spArrayCorrerEsquerda[this.indiceCorrer].width;
					var aY_2=this.y+this.spArrayCorrerEsquerda[this.indiceCorrer].height;
					//ColisaoComInimigos
					for(i=0;i<ArrayInimigos.length;i++){
							if(!ArrayInimigos[i].vivo)
								continue;
							var iX_1=ArrayInimigos[i].x;
							var iY_1=ArrayInimigos[i].y;
							var iX_2=iX_1+ArrayInimigos[i].width;
							var iY_2=iY_1+ArrayInimigos[i].height;
							if(aX_1 > iX_2 || iX_1 > aX_2){
								continue;
							}
							if(aY_1 > iY_2 || iY_1 > aY_2){
								continue;
							}
							if(this.verificaImageData(ArrayInimigos[i].getSprite(),ArrayInimigos[i].x,ArrayInimigos[i].y,this.spArrayCorrerEsquerda[this.indiceCorrer],ctx)){
								this.morreu=true;
								return;
							}
					}
					for(i=0;i<coordenadasComColisao.length;i+=4){
						if(coordenadasComColisao[i+3]){
							var bX_1=coordenadasComColisao[i+1];
							var bY_1=coordenadasComColisao[i+2];
							var bX_2=coordenadasComColisao[i+1] + ArrayObjetosColisao[coordenadasComColisao[i]].width;
							var bY_2=coordenadasComColisao[i+2]+ ArrayObjetosColisao[coordenadasComColisao[i]].height;
							if(aX_1 > bX_2 || bX_1 > aX_2){
								continue;
							}
							if(aY_1 > bY_2 || bY_1 > aY_2){
								continue;
							}
							if(this.verificaImageData(ArrayObjetosColisao[coordenadasComColisao[i]],coordenadasComColisao[i+1],coordenadasComColisao[i+2],this.spArrayCorrerEsquerda[this.indiceCorrer],ctx)){
								return coordenadasComColisao[i];
							}
			    	}
					}
				}
		}
		else if(this.saltar){
				if(this.orientacao){
						var aX_1=this.x
						var aY_1=this.y
						var aX_2=this.x+this.spArraySaltarDireita[this.indiceSaltar].width;
						var aY_2=this.y+this.spArraySaltarDireita[this.indiceSaltar].height;
						//ColisaoComInimigos
						for(i=0;i<ArrayInimigos.length;i++){
								if(!ArrayInimigos[i].vivo)
									continue;
								var iX_1=ArrayInimigos[i].x;
								var iY_1=ArrayInimigos[i].y;
								var iX_2=iX_1+ArrayInimigos[i].width;
								var iY_2=iY_1+ArrayInimigos[i].height;
								if(aX_1 > iX_2 || iX_1 > aX_2){
									continue;
								}
								if(aY_1 > iY_2 || iY_1 > aY_2){
									continue;
								}
								if(this.verificaImageData(ArrayInimigos[i].getSprite(),ArrayInimigos[i].x,ArrayInimigos[i].y,this.spArraySaltarDireita[this.indiceSaltar],ctx)){
									this.morreu=true;
									return;
								}
						}
						for(i=0;i<coordenadasComColisao.length;i+=4){
							if(coordenadasComColisao[i+3]){
								var bX_1=coordenadasComColisao[i+1];
								var bY_1=coordenadasComColisao[i+2];
								var bX_2=coordenadasComColisao[i+1] + ArrayObjetosColisao[coordenadasComColisao[i]].width;
								var bY_2=coordenadasComColisao[i+2]+ ArrayObjetosColisao[coordenadasComColisao[i]].height;
								if(aX_1 > bX_2 || bX_1 > aX_2){
									continue;
								}
								if(aY_1 > bY_2 || bY_1 > aY_2){
									continue;
								}
								if(this.verificaImageData(ArrayObjetosColisao[coordenadasComColisao[i]],coordenadasComColisao[i+1],coordenadasComColisao[i+2],this.spArraySaltarDireita[this.indiceSaltar],ctx)){
									return coordenadasComColisao[i];
								}
							}
						}
				}
				else{
					var aX_1=this.x
					var aY_1=this.y
					var aX_2=this.x+this.spArraySaltarEsquerda[this.indiceSaltar].width;
					var aY_2=this.y+this.spArraySaltarEsquerda[this.indiceSaltar].height;
					//ColisaoComInimigos
					for(i=0;i<ArrayInimigos.length;i++){
							if(!ArrayInimigos[i].vivo)
								continue;
							var iX_1=ArrayInimigos[i].x;
							var iY_1=ArrayInimigos[i].y;
							var iX_2=iX_1+ArrayInimigos[i].width;
							var iY_2=iY_1+ArrayInimigos[i].height
							if(aX_1 > iX_2 || iX_1 > aX_2){
								continue;
							}
							if(aY_1 > iY_2 || iY_1 > aY_2){
								continue;
							}
							if(this.verificaImageData(ArrayInimigos[i].getSprite(),ArrayInimigos[i].x,ArrayInimigos[i].y,this.spArraySaltarEsquerda[this.indiceSaltar],ctx)){
								this.morreu=true;
								return;
							}
					}
					for(i=0;i<coordenadasComColisao.length;i+=4){
						if(coordenadasComColisao[i+3]){
							var bX_1=coordenadasComColisao[i+1];
							var bY_1=coordenadasComColisao[i+2];
							var bX_2=coordenadasComColisao[i+1] + ArrayObjetosColisao[coordenadasComColisao[i]].width;
							var bY_2=coordenadasComColisao[i+2]+ ArrayObjetosColisao[coordenadasComColisao[i]].height;
							if(aX_1 > bX_2 || bX_1 > aX_2){
								continue;
							}
							if(aY_1 > bY_2 || bY_1 > aY_2){
								continue;
							}
							if(this.verificaImageData(ArrayObjetosColisao[coordenadasComColisao[i]],coordenadasComColisao[i+1],coordenadasComColisao[i+2],this.spArraySaltarEsquerda[this.indiceSaltar],ctx)){
								return coordenadasComColisao[i];
							}
						}
					}
				}
		}
		else if(this.deslizar){
				if(this.orientacao){
						var aX_1=this.x;
						var aY_1=this.y;
						var aX_2=this.x+this.spArrayDeslizarDireita[this.indiceDeslizar].width;
						var aY_2=this.y+this.spArrayDeslizarDireita[this.indiceDeslizar].height;
						//ColisaoComInimigos
						for(i=0;i<ArrayInimigos.length;i++){
								if(!ArrayInimigos[i].vivo)
									continue;
								var iX_1=ArrayInimigos[i].x;
								var iY_1=ArrayInimigos[i].y;
								var iX_2=iX_1+ArrayInimigos[i].width;
								var iY_2=iY_1+ArrayInimigos[i].height;
								if(aX_1 > iX_2 || iX_1 > aX_2){
									continue;
								}
								if(aY_1 > iY_2 || iY_1 > aY_2){
									continue;
								}
								if(this.verificaImageData(ArrayInimigos[i].getSprite(),ArrayInimigos[i].x,ArrayInimigos[i].y,this.spArrayDeslizarDireita[this.indiceDeslizar],ctx)){
									ArrayInimigos[i].vivo=false;
								}
						}
						for(i=0;i<coordenadasComColisao.length;i+=4){
							if(coordenadasComColisao[i+3]){
								var bX_1=coordenadasComColisao[i+1];
								var bY_1=coordenadasComColisao[i+2];
								var bX_2=coordenadasComColisao[i+1] + ArrayObjetosColisao[coordenadasComColisao[i]].width;
								var bY_2=coordenadasComColisao[i+2]+ ArrayObjetosColisao[coordenadasComColisao[i]].height;
								if(aX_1 > bX_2 || bX_1 > aX_2){
									continue;
								}
								if(aY_1 > bY_2 || bY_1 > aY_2){
									continue;
								}
								if(this.verificaImageData(ArrayObjetosColisao[coordenadasComColisao[i]],coordenadasComColisao[i+1],coordenadasComColisao[i+2],this.spArrayDeslizarDireita[this.indiceDeslizar],ctx)){
									return coordenadasComColisao[i];
								}
							}
						}
				}
				else{
					var aX_1=this.x;
					var aY_1=this.y;
					var aX_2=this.x+this.spArrayDeslizarEsquerda[this.indiceDeslizar].width;
					var aY_2=this.y+this.spArrayDeslizarEsquerda[this.indiceDeslizar].height;
					//ColisaoComInimigos
					for(i=0;i<ArrayInimigos.length;i++){
							if(!ArrayInimigos[i].vivo)
								continue;
							var iX_1=ArrayInimigos[i].x;
							var iY_1=ArrayInimigos[i].y;
							var iX_2=iX_1+ArrayInimigos[i].width;
							var iY_2=iY_1+ArrayInimigos[i].height;
							if(aX_1 > iX_2 || iX_1 > aX_2){
								continue;
							}
							if(aY_1 > iY_2 || iY_1 > aY_2){
								continue;
							}
							if(this.verificaImageData(ArrayInimigos[i].getSprite(),ArrayInimigos[i].x,ArrayInimigos[i].y,this.spArrayDeslizarEsquerda[this.indiceDeslizar],ctx)){
								ArrayInimigos[i].vivo=false;
							}
					}
					for(i=0;i<coordenadasComColisao.length;i+=4){
						if(coordenadasComColisao[i+3]){
							var bX_1=coordenadasComColisao[i+1];
							var bY_1=coordenadasComColisao[i+2];
							var bX_2=coordenadasComColisao[i+1] + ArrayObjetosColisao[coordenadasComColisao[i]].width;
							var bY_2=coordenadasComColisao[i+2]+ ArrayObjetosColisao[coordenadasComColisao[i]].height;
							if(aX_1 > bX_2 || bX_1 > aX_2){
								continue;
							}
							if(aY_1 > bY_2 || bY_1 > aY_2){
								continue;
							}
						 	if(this.verificaImageData(ArrayObjetosColisao[coordenadasComColisao[i]],coordenadasComColisao[i+1],coordenadasComColisao[i+2],this.spArrayDeslizarEsquerda[this.indiceDeslizar],ctx)){
								return coordenadasComColisao[i];
							}
						}
					}
				}
		}
		return -1;
	}

	verificaImageData(objeto,objetoX,objetoY,personagem,ctx)
	{
		var Cx_1 = Math.round(Math.max(objetoX,personagem.x));
		var Cy_1 = Math.round(Math.max(objetoY,personagem.y));
		var Cx_2 = Math.round(Math.min(objetoX + objeto.width,personagem.x + personagem.width));
		var Cy_2 = Math.round(Math.min(objetoY + objeto.height,personagem.y + personagem.height));
		var xInicial=Cx_1;
		while(Cx_1 <=Cx_2 && Cy_1 <= Cy_2 ){
			var xO=Math.round(Math.abs(Cx_1-objetoX));
			var yO=Math.round(Math.abs(Cy_1-objetoY));
			var xP=Math.round(Math.abs(Cx_1-personagem.x));
			var yP=Math.round(Math.abs(Cy_1-personagem.y));
			var TransparenciaObjeto=objeto.imageData.data[(yO*objeto.width*4)+(xO*4)+3];
			var TransparenciaPersonagem=personagem.imageData.data[(yP*personagem.width*4)+(xP*4)+3];
			if(TransparenciaObjeto>0 && TransparenciaPersonagem>0){
				return true;
			}
			if(Cx_1==Cx_2 && Cy_1 < Cy_2){
				Cx_1=xInicial;
				Cy_1++;
			}
			else if(Cx_1==Cx_2 && Cy_1 == Cy_2){
				return false;
			}
			else{
				Cx_1++;
			}
		}
	}
}
