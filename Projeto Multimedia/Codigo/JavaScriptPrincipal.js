"use strict";

(function()
{
	window.addEventListener("load", main);
}());
//Imagens Personagem
const numeroImagensParado=10;
const numeroImagensCorrer=10;
const numeroImagensMorrer=10;
const numeroImagensDeslizar=10;
const numeroTotalImagensPersonagem=2*numeroImagensCorrer+2*numeroImagensParado+2*numeroImagensMorrer+2*numeroImagensDeslizar;

//Imagens Niveis
const numeroBackgrounds=1;
const miniBackgrounds=2;
const botoesNivel=5;
const numeroBlocosNivel=18;
const numeroObjetosColisaoNivel=7;
const numeroObjetosSemColisaoNivel=11;
const numeroTotalImagensNivel=numeroBlocosNivel+numeroObjetosColisaoNivel+numeroObjetosSemColisaoNivel+numeroBackgrounds+miniBackgrounds+botoesNivel;

//Imagens Inimigos
const numeroImagensInimigo=3;
const numeroTotalImagensInimigo=numeroImagensInimigo*2;
const comprimentoInimigo=46;
const alturaInimigo=31;

const numetoTotalImagensMenu=21;
const numeroTotalMusicas=5;

const numeroColunas=20;
const numeroLinhas=15;


const numeroNiveis=3;

function main()
{
  var canvas=document.getElementById("canvas");
  var ctx=canvas.getContext("2d");
	canvas.addEventListener("MusicasCarregadas",CarregaMenus);
	canvas.addEventListener("menuCarregado",CarregaNiveis);
	canvas.addEventListener("ImagensNiveisCarregadas",LoadInimigo);
	canvas.addEventListener("InimigosCarregados",LoadPersonagem);
	canvas.addEventListener("initend",initEndHandler);
	canvas.addEventListener("nivelEscolhido",Start);

	var numMenu=[0]; //saber o menu

	var menuIni=new Menu(ctx); //Menu 0
	var menuOpt=new Menu(ctx); // Menu 1
	var menuRank=new Menu(ctx); // Menu 2
	var menuAjuda=new Menu(ctx); //Menu 3
	var menuNiveis=new Menu(ctx); // Menu 4

	var ArrayMenus=[menuIni,menuOpt,menuRank,menuAjuda,menuNiveis];

	var ArrayMusicas=new Array();

	//Nivel0
	var dataNivel0=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 16, 0, 0, 0, 0, 0, 4, 5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 9, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 13, 14, 15, 0, 0, 0, 0, 0, 0, 0, 4, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13, 14, 15, 0, 0, 0, 4, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 10, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 17, 17, 17, 5, 5, 2, 2, 2, 2, 2, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 18, 18, 18, 5, 5, 5, 5, 5, 5, 5, 5];
  var semColisaoNivel0=[8,200,22,6,265,136,5,55,479,2,303,414,7,746,101];
  var colisaoNivel0=[1,84,327,true,2,228,142,true,3,753,70,true,6,749,488,true];
  var BlocoMovel0=new BlocoMovel(14,600,160,true,280,true,1);
  var ArrayBlocosMoveis0=[BlocoMovel0];
	var ArrayInimigos0=new Array();
  var Nivel0=new Nivel(ctx,dataNivel0,semColisaoNivel0,colisaoNivel0,0,canvas.height-80-88,ArrayBlocosMoveis0,numeroLinhas,numeroColunas,ArrayInimigos0);

	//Nivel 1
	var dataNivel1=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 0, 0, 9, 9, 9, 9, 16, 0, 0, 0, 0, 13, 14, 15, 0, 0, 0, 0, 4, 5, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 9, 9, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 13, 14, 14, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13, 14, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2, 3, 0, 0, 0, 0, 0, 0, 2, 2, 2, 17, 17, 17, 8, 5, 5, 5, 5, 5, 5, 5, 2, 2, 17, 17, 17, 17, 5, 5, 5, 18, 18, 18, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 18, 18, 18, 18];
  var BlocoMovel1=new BlocoMovel(13,240,160,true,160,true,1);
  var BlocoMovel2=new BlocoMovel(15,280,160,true,160,true,1);
  var semColisaoNivel1=[5,45,479,0,561,491,9,373,353,6,117,296,10,3,70,3,431,144,8,700,22];
  var colisaoNivel1=[0,607,487,true,1,520,247,true,5,720,368,true,4,51,270,true,2,57,101,true,6,761,127,true];
  var ArrayBlocosMoveis1=[BlocoMovel1,BlocoMovel2];
	var inimigo=new Inimigo(240,480-alturaInimigo,comprimentoInimigo,alturaInimigo,320-comprimentoInimigo,true,1);
	var ArrayInimigos1=[inimigo];
  var Nivel1=new Nivel(ctx,dataNivel1,semColisaoNivel1,colisaoNivel1,0,canvas.height-80-88,ArrayBlocosMoveis1,numeroLinhas,numeroColunas,ArrayInimigos1);

	//Nivel 3
	var dataNivel2=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 0, 0, 0, 0, 0, 0, 0, 1, 3, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 16, 0, 0, 0, 0, 0, 0, 0, 12, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18];
	var BlocoMovel3=new BlocoMovel(15,640,480,false,200,false,1);
	var BlocoMovel4=new BlocoMovel(13,600,480,false,200,false,1);
	var BlocoMovel5=new BlocoMovel(13,120,440,false,200,true,1);
	var BlocoMovel6=new BlocoMovel(15,160,440,false,200,true,1);
	var BlocoMovel7=new BlocoMovel(15,400,280,false,280,false,2);
	var BlocoMovel8=new BlocoMovel(13,360,280,false,280,false,2);
	var ArrayBlocosMoveis2=[BlocoMovel3,BlocoMovel4,BlocoMovel5,BlocoMovel6,BlocoMovel7,BlocoMovel8];
	var inimigo1=new Inimigo(40,120-alturaInimigo,comprimentoInimigo,alturaInimigo,160,true,1);
	var inimigo2=new Inimigo(400,120-alturaInimigo,comprimentoInimigo,alturaInimigo,160,false,1);
	var ArrayInimigos2=[inimigo1,inimigo2];
	var semColisaoNivel2=[2,743,414,6,730,96,4,33,319,0,44,91,7,293,101,3,312,104];
	var ColisaoNivel2=[6,5,87,true,1,481,327,true,0,641,207,true,2,3,341,true,5,750,89,true];
	var Nivel2=new Nivel(ctx,dataNivel2,semColisaoNivel2,ColisaoNivel2,760,300,ArrayBlocosMoveis2,numeroLinhas,numeroColunas,ArrayInimigos2);


	var ArrayTodosInimigos=[inimigo,inimigo1,inimigo2];
	var ArrayNiveis=[Nivel0,Nivel1,Nivel2];

	var personagem= new Personagem(0,0,1);
	var esquema = [1]; //esquema 1 = setas    esquema 2 = wasd

	var mouseClick=[false];
	var mouseClickPosition=[0,0];
	var mousePosition=[0,0];
	var keyState=[false,false,false,false];

	carregaMusicas(ArrayMusicas,ctx);
	function CarregaMenus(ev){
		carregaImagens(ctx,ArrayMenus,ArrayMusicas);
	}

	function CarregaNiveis(ev){
			carregaImagensNiveis(ctx,ArrayNiveis);
	}

	function initEndHandler(ev){
  	canvas.addEventListener("click",cch);
		canvas.addEventListener("click",cch);
		canvas.addEventListener("mousemove",mmh);
		ArrayMusicas[0].play();
		AnimacaoMenu(0,ctx,numMenu,ArrayMenus,mouseClick,mouseClickPosition,mousePosition,ArrayNiveis,ArrayMusicas,esquema);
	}
	var cch = function(ev){
		canvasClickHandler(ev,mouseClickPosition,mouseClick);
	}
	var mmh= function(ev){
		canvasMouseMoveHandler(ev,mousePosition);
	}

	function Start(ev){
		window.addEventListener("keydown",ckdh);
		window.addEventListener("keyup",ckuh);
		var indiceNivelEscolhido=ev.detail.nivel;
		personagem.posicaoIncialNivel(ArrayNiveis[indiceNivelEscolhido-1]);
		IniciaAnimacao(ctx,personagem,0,ArrayNiveis[indiceNivelEscolhido-1],keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,0);
	}
	function LoadInimigo(ev){
		canvas.removeEventListener("menuCarregado",CarregaNiveis);
		carregaInimigo(ctx,ArrayTodosInimigos);
	}
 	function LoadPersonagem(ev){
		canvas.removeEventListener("ImagensNiveisCarregadas",LoadInimigo);
		canvas.removeEventListener("InimigosCarregados",LoadPersonagem);
		carregaPersonagem(ctx,personagem);
	}
	function ckdh (ev)
	{
		canvasKeyDownHandler(ev,keyState,esquema);
	}
	function ckuh(ev)
	{
		canvasKeyUpHandler(ev,keyState,personagem,esquema);
	}

}
function carregaMusicas(ArrayMusicas,ctx){
	var numCarregadas=0;
	var MusicaMenu= new Audio();
	MusicaMenu.addEventListener("canplaythrough",MusicLoadedHandler);
	MusicaMenu.src="Musica/MusicaMenu.mp3"
	MusicaMenu.volume=0.1;
	MusicaMenu.id="MusicaMenu";

	var soundEfect= new Audio();
	soundEfect.addEventListener("canplaythrough",MusicLoadedHandler);
	soundEfect.src="Musica/SoundEfectBotoes.mp3";
	soundEfect.volume=1;
	soundEfect.id="somBotao";

	var pickUpSound=new Audio();
	pickUpSound.addEventListener("canplaythrough",MusicLoadedHandler);
	pickUpSound.src="Musica/pickupSound.mp3";
	pickUpSound.volume=1;
	pickUpSound.id="pickup";

	var musicaLevels=new Audio();
	musicaLevels.addEventListener("canplaythrough",MusicLoadedHandler);
	musicaLevels.src="Musica/musicaLevel.mp3";
	musicaLevels.volume=0.2;
	musicaLevels.id="musicaLevels";

	var jumpSound=new Audio();
	jumpSound.addEventListener("canplaythrough",MusicLoadedHandler);
	jumpSound.src="Musica/salto.mp3";
	jumpSound.volume=0.2;
	jumpSound.id="jumpSound";

	function MusicLoadedHandler(ev)
	{
		var musica=ev.target;
		if(musica.id=="MusicaMenu"){
			musica.addEventListener('ended', function() {
		  	musica.currentTime = 0;
		    musica.play();
			}, false);
			ArrayMusicas[0]=musica;
		}
		else if(musica.id=="somBotao"){
			ArrayMusicas[1]=musica;
		}
		else if(musica.id=="musicaLevels"){
			musica.addEventListener('ended', function() {
		  	musica.currentTime = 0;
		    musica.play();
			}, false);
			ArrayMusicas[2]=musica;
		}
		else if(musica.id=="pickup"){
			ArrayMusicas[3]=musica;
		}
		else if(musica.id=="jumpSound"){
			ArrayMusicas[4]=musica;
		}
		numCarregadas++;
		if(numCarregadas==numeroTotalMusicas){
			var ev2 = new Event("MusicasCarregadas");
			ctx.canvas.dispatchEvent(ev2);
		}
	}


}
function carregaImagens(ctx,ArrayMenus){
		var numCarregadas=0;
		var Titulo= new Image();
		Titulo.addEventListener("load",imgLoadedHandlerMenu);
	  Titulo.id="titulo";
	  Titulo.src="BackGrounds/BackGroundLetras.png";

		var Play = new Image();
	  Play.addEventListener("load",imgLoadedHandlerMenu);
	  Play.id="play";
	  Play.src="Buttons/Play.png";

	  var Options = new Image();
	  Options.addEventListener("load",imgLoadedHandlerMenu);
	  Options.id="options";
	  Options.src="Buttons/Options.png";

	  var Ranking = new Image();
	  Ranking.addEventListener("load",imgLoadedHandlerMenu);
	  Ranking.id="ranking";
	  Ranking.src="Buttons/Ranking.png";

	  var Help = new Image();
	  Help.addEventListener("load",imgLoadedHandlerMenu);
	  Help.id="help";
	  Help.src="Buttons/Help.png";

	  var Quit = new Image();
	  Quit.addEventListener("load",imgLoadedHandlerMenu);
	  Quit.id="quit";
	  Quit.src="Buttons/Quit.png";

	  var Boneco=new Image();
	  Boneco.addEventListener("load",imgLoadedHandlerMenu);
	  Boneco.id="boneco";
	  Boneco.src="BackGrounds/BonecoMenu.png";

		var Back=new Image();
	  Back.addEventListener("load",imgLoadedHandlerMenu);
	  Back.id="back";
	  Back.src="Buttons/Back.png";

		var aumentaVolume=new Image();
		aumentaVolume.addEventListener("load",imgLoadedHandlerMenu);
	  aumentaVolume.id="aumentaVol";
	  aumentaVolume.src="Buttons/Sound+.png";

		var diminuiVolume=new Image();
		diminuiVolume.addEventListener("load",imgLoadedHandlerMenu);
	  diminuiVolume.id="diminuiVol";
	  diminuiVolume.src="Buttons/Sound-.png";

		var somOn= new Image();
		somOn.addEventListener("load",imgLoadedHandlerMenu);
	  somOn.id="somOn";
	 	somOn.src="Buttons/SomOn.png";

		var somOff= new Image();
		somOff.addEventListener("load",imgLoadedHandlerMenu);
	  somOff.id="somOff";
	  somOff.src="Buttons/SomOff.png";

		var FundoAjuda=new Image();
	  FundoAjuda.addEventListener("load",imgLoadedHandlerMenu);
	  FundoAjuda.id="fundoAjuda";
	  FundoAjuda.src="BackGrounds/AjudaMenu.png";

		var FundoRank=new Image();
	  FundoRank.addEventListener("load",imgLoadedHandlerMenu);
	  FundoRank.id="fundoRank";
	  FundoRank.src="BackGrounds/rankingMenu.png";

		var FundoOpcoes=new Image();
	  FundoOpcoes.addEventListener("load",imgLoadedHandlerMenu);
	  FundoOpcoes.id="fundoOpcoes";
	  FundoOpcoes.src="BackGrounds/optionsMenu.png";

		var FundoLevels=new Image();
		FundoLevels.addEventListener("load",imgLoadedHandlerMenu);
	  FundoLevels.id="fundoLevels";
	  FundoLevels.src="BackGrounds/levelsMenu.png";

		var selecionaOpcao=new Image();
		selecionaOpcao.addEventListener("load",imgLoadedHandlerMenu);
		selecionaOpcao.id="seleciona";
		selecionaOpcao.src="Buttons/Select.png";

		var selecionaClaroOpcao=new Image();
		selecionaClaroOpcao.addEventListener("load",imgLoadedHandlerMenu);
		selecionaClaroOpcao.id="selecionaClaro";
		selecionaClaroOpcao.src="Buttons/SelectClaro.png";

		var arrayBotoesNiveis=new Array();
		for(var i=1;i<=numeroNiveis;i++){
			arrayBotoesNiveis[i]=new Image();
			arrayBotoesNiveis[i].addEventListener("load",imgLoadedHandlerMenu);
			arrayBotoesNiveis[i].id="nivel"+i;
			arrayBotoesNiveis[i].src="Buttons/Nivel"+i+".png";
		}

		function imgLoadedHandlerMenu(ev)
	  {
	    var imagem=ev.target;
			if(imagem.id=="titulo"){
	      var sp= new BotaoMenu(0,0,imagem.width,imagem.height,imagem);
	      ArrayMenus[0].spArray[6]=sp;
	    }
	   	else if(imagem.id=="play"){
	      var sp= new BotaoMenu(300,150,Math.round(imagem.width/1.7),Math.round(imagem.height/1.7),imagem);
	      ArrayMenus[0].spArray[0]=sp;
	    }
	    else if(imagem.id=="options"){
	      var sp= new BotaoMenu(300,225,Math.round(imagem.width/1.7),Math.round(imagem.height/1.7),imagem);
	      ArrayMenus[0].spArray[1]=sp;
	    }
	    else if(imagem.id=="ranking"){
	      var sp= new BotaoMenu(300,300,Math.round(imagem.width/1.7),Math.round(imagem.height/1.7),imagem);
	      ArrayMenus[0].spArray[2]=sp;
	    }
	    else if(imagem.id=="help"){
	      var sp= new BotaoMenu(300,375,Math.round(imagem.width/1.7),Math.round(imagem.height/1.7),imagem);
	      ArrayMenus[0].spArray[3]=sp;
	    }
	    else if(imagem.id=="quit"){
	      var sp= new BotaoMenu(600,525,Math.round(imagem.width/1.7),Math.round(imagem.height/1.7),imagem);
	      ArrayMenus[0].spArray[4]=sp;
	    }
	    else if(imagem.id=="boneco"){
	      var sp= new BotaoMenu(30,225,Math.round(imagem.width/1.75),Math.round(imagem.height/1.75),imagem);
	      ArrayMenus[0].spArray[5]=sp;
	    }
			else if(imagem.id=="back"){
	      var sp= new BotaoMenu(15,525,Math.round(imagem.width/1.7),Math.round(imagem.height/1.7),imagem);
	      ArrayMenus[3].spArray[0]=sp;
				ArrayMenus[1].spArray[0]=sp;
				ArrayMenus[2].spArray[0]=sp;
				ArrayMenus[4].spArray[0]=sp;
	    }
			else if(imagem.id=="fundoAjuda"){
	      var sp= new BotaoMenu(0,0,imagem.width,imagem.height,imagem);
	      ArrayMenus[3].spArray[1]=sp;
	    }
			else if(imagem.id=="fundoRank"){
	      var sp= new BotaoMenu(0,0,imagem.width,imagem.height,imagem);
	      ArrayMenus[2].spArray[1]=sp;
	    }
			else if(imagem.id=="fundoOpcoes"){
	      var sp= new BotaoMenu(0,0,imagem.width,imagem.height,imagem);
	      ArrayMenus[1].spArray[1]=sp;

	    }
			else if(imagem.id=="fundoLevels"){
				var sp= new BotaoMenu(0,0,imagem.width,imagem.height,imagem);
	      ArrayMenus[4].spArray[1]=sp;
			}
			else if(imagem.id=="aumentaVol"){
	      var sp= new BotaoMenu(365,200,Math.round(imagem.width/1.75),Math.round(imagem.height/1.75),imagem);
	      ArrayMenus[1].spArray[2]=sp;
	    }
			else if(imagem.id=="diminuiVol"){
	      var sp= new BotaoMenu(425,200,Math.round(imagem.width/1.75),Math.round(imagem.height/1.75),imagem);
	      ArrayMenus[1].spArray[3]=sp;
	    }
			else if(imagem.id=="somOff"){
	      var sp= new BotaoMenu(480,200,Math.round(imagem.width/1.75),Math.round(imagem.height/1.75),imagem);
	      ArrayMenus[1].spArray[4]=sp;
	    }
			else if(imagem.id=="somOn"){
	      var sp= new BotaoMenu(310,200,Math.round(imagem.width/1.75),Math.round(imagem.height/1.75),imagem);
	      ArrayMenus[1].spArray[5]=sp;
	    }
			else if(imagem.id=="seleciona"){
	      var sp= new BotaoMenu(310,325,Math.round(imagem.width/1.9),Math.round(imagem.height/1.9),imagem);
	      ArrayMenus[1].spArray[8]=sp;
				var sp2= new BotaoMenu(510,325,Math.round(imagem.width/1.9),Math.round(imagem.height/1.9),imagem);
				ArrayMenus[1].spArray[6]=sp2;
	    }
			else if(imagem.id=="selecionaClaro"){
	      var sp= new BotaoMenu(310,325,Math.round(imagem.width/1.9),Math.round(imagem.height/1.9),imagem);
	      ArrayMenus[1].spArray[7]=sp;
				var sp2= new BotaoMenu(510,325,Math.round(imagem.width/1.9),Math.round(imagem.height/1.9),imagem);
				ArrayMenus[1].spArray[9]=sp2;
	    }
			else if(imagem.id=="nivel1"){
				var sp= new BotaoMenu(322,150,Math.round(imagem.width/1.7),Math.round(imagem.height/1.7),imagem);
				ArrayMenus[4].spArray[2]=sp;
			}
			else if(imagem.id=="nivel2"){
				var sp= new BotaoMenu(322,225,Math.round(imagem.width/1.7),Math.round(imagem.height/1.7),imagem);
				ArrayMenus[4].spArray[3]=sp
			}
			else if(imagem.id=="nivel3"){
				var sp= new BotaoMenu(322,300,Math.round(imagem.width/1.7),Math.round(imagem.height/1.7),imagem);
				ArrayMenus[4].spArray[4]=sp
			}
			numCarregadas++;
			if(numCarregadas==numetoTotalImagensMenu){
				var ev2 = new Event("menuCarregado");
				ctx.canvas.dispatchEvent(ev2);
			}
		}
}
function canvasClickHandler(ev,mouseClickPosition,mouseClick){
	mouseClick[0]=true;
	mouseClickPosition[0]=ev.offsetX;
	mouseClickPosition[1]=ev.offsetY;
}
function canvasMouseMoveHandler(ev,mousePosition){
	mousePosition[0]=ev.offsetX;
	mousePosition[1]=ev.offsetY;
}
function carregaImagensNiveis(ctx,ArrayNiveis){
	var numeroCarregadas=0;

	var BackGround = new Image();
	BackGround.addEventListener("load",imgLoadedHandlerNivel);
	BackGround.id="background";
	BackGround.src="BackGrounds/BG.png";

	var Pausa=new Image();
	Pausa.addEventListener("load",imgLoadedHandlerNivel);
	Pausa.id="pausa";
	Pausa.src="Niveis/BG/paused.png";

	var NivelCompleto=new Image();
	NivelCompleto.addEventListener("load",imgLoadedHandlerNivel);
	NivelCompleto.id="nivelcompleto";
	NivelCompleto.src="Niveis/BG/LevelCompleted.png";

	var BotaoNext=new Image();
	BotaoNext.addEventListener("load",imgLoadedHandlerNivel);
	BotaoNext.id="botaoNext";
	BotaoNext.src="Buttons/next.png";

	var BotaoPausa=new Image();
	BotaoPausa.addEventListener("load",imgLoadedHandlerNivel);
	BotaoPausa.id="botaoPausa1";
	BotaoPausa.src="Buttons/pausa.png";

	var BotaoResume=new Image();
	BotaoResume.addEventListener("load",imgLoadedHandlerNivel);
	BotaoResume.id="botaoResume2";
	BotaoResume.src="Buttons/resume.png";

	var BotaoRestart=new Image();
	BotaoRestart.addEventListener("load",imgLoadedHandlerNivel);
	BotaoRestart.id="botaoRestart3";
	BotaoRestart.src="Buttons/restart.png";

	var BotaoQuit=new Image();
	BotaoQuit.addEventListener("load",imgLoadedHandlerNivel);
	BotaoQuit.id="botaoQuit4";
	BotaoQuit.src="Buttons/quitInGame.png";

	var i;
	var arrayBlocosNivel=new Array();
	for(i=1;i<=numeroBlocosNivel;i++){
		arrayBlocosNivel[i]=new Image();
		arrayBlocosNivel[i].addEventListener("load",imgLoadedHandlerNivel);
		arrayBlocosNivel[i].id=""+i;
		arrayBlocosNivel[i].src="Niveis/Blocos/"+i+".png";
	}
	var arrayObjetosColisao= new Array();
	for(i=0;i<numeroObjetosColisaoNivel;i++){
		arrayObjetosColisao[i]=new Image();
		arrayObjetosColisao[i].addEventListener("load",imgLoadedHandlerNivel);
		arrayObjetosColisao[i].id="colisao"+i;
		arrayObjetosColisao[i].src="Niveis/ObjetosColisao/Colisao"+i+".png";
	}
	var arrayObjetosSemColisao= new Array();
	for(i=0;i<numeroObjetosSemColisaoNivel;i++){
		arrayObjetosColisao[i]=new Image();
		arrayObjetosColisao[i].addEventListener("load",imgLoadedHandlerNivel);
		arrayObjetosColisao[i].id="SemColisao"+i;
		arrayObjetosColisao[i].src="Niveis/ObjetosSemColisao/SemColisao"+i+".png";
	}

	function imgLoadedHandlerNivel(ev)
	{
		var imagem=ev.target;
		var i;
		if(imagem.id=="background"){
			for(i=0;i<ArrayNiveis.length;i++){
				ArrayNiveis[i].fundo=BackGround;
			}
		}
		else if(imagem.id.slice(0,-1)=="colisao"){
				for(i=0;i<ArrayNiveis.length;i++){
					var sp1= new SpriteImage(0,0,imagem.width,imagem.height,imagem);
					sp1.getDadosImagem();
					ArrayNiveis[i].ArrayObjetosColisao[parseInt(imagem.id.slice(-1))]=sp1;
				}
		}
		else if(imagem.id.slice(0,-1)=="SemColisao"){
				for(i=0;i<ArrayNiveis.length;i++){
					var sp= new SpriteImage(0,0,imagem.width,imagem.height,imagem);
					ArrayNiveis[i].ArrayObjetosSemColisao[parseInt(imagem.id.slice(-1))]=sp;
				}
		}
		else if(imagem.id.slice(0,-2)=="SemColisao"){
				for(i=0;i<ArrayNiveis.length;i++){
					var sp= new SpriteImage(0,0,imagem.width,imagem.height,imagem);
					ArrayNiveis[i].ArrayObjetosSemColisao[parseInt(imagem.id.slice(-2))]=sp;
				}
		}
		else if(imagem.id=="pausa"){
			for(i=0;i<ArrayNiveis.length;i++){
				var sp1= new SpriteImage(272,127,imagem.width,imagem.height,imagem);
				ArrayNiveis[i].ArrayPaused.push(sp1);
			}
		}
		else if(imagem.id=="nivelcompleto"){
			for(i=0;i<ArrayNiveis.length;i++){
				var sp1= new SpriteImage(250,150,imagem.width,imagem.height,imagem);
				ArrayNiveis[i].ArrayLevelCompleted[0]=sp1;
			}

		}
		else if(imagem.id=="botaoNext"){
			for(i=0;i<ArrayNiveis.length;i++){
				var sp1= new BotaoMenu(320,370,imagem.width,imagem.height,imagem);
				ArrayNiveis[i].ArrayLevelCompleted[1]=sp1;
			}
		}
		else if(imagem.id.slice(0,-1)=="botaoPausa"){
			for(i=0;i<ArrayNiveis.length;i++){
				var sp1= new BotaoMenu(755,10,imagem.width,imagem.height,imagem);
				ArrayNiveis[i].ArrayPaused[imagem.id.slice(-1)]=sp1;
			}
		}
		else if(imagem.id.slice(0,-1)=="botaoResume"){
			for(i=0;i<ArrayNiveis.length;i++){
				var sp1= new BotaoMenu(320,215,imagem.width,imagem.height,imagem);
				ArrayNiveis[i].ArrayPaused[imagem.id.slice(-1)]=sp1;
			}
		}
		else if(imagem.id.slice(0,-1)=="botaoRestart"){
			for(i=0;i<ArrayNiveis.length;i++){
				var sp1= new BotaoMenu(320,285,imagem.width,imagem.height,imagem);
				ArrayNiveis[i].ArrayPaused[imagem.id.slice(-1)]=sp1;
			}
		}
		else if(imagem.id.slice(0,-1)=="botaoQuit"){
			for(i=0;i<ArrayNiveis.length;i++){
				var sp1= new BotaoMenu(320,355,imagem.width,imagem.height,imagem);
				ArrayNiveis[i].ArrayPaused[imagem.id.slice(-1)]=sp1;
			}
		}
		else{
			for(i=0;i<ArrayNiveis.length;i++){
				var sp=new SpriteImage(0,0,imagem.width,imagem.height,imagem);
				ArrayNiveis[i].ArrayBlocos[parseInt(imagem.id)-1]=sp;
			}
		}
		numeroCarregadas++;
		if(numeroCarregadas==numeroTotalImagensNivel){
			for(i=0;i<ArrayNiveis.length;i++){
				ArrayNiveis[i].atribuiCoordenadas();
			}
			var ev2 = new Event("ImagensNiveisCarregadas");
			ctx.canvas.dispatchEvent(ev2);
		}

	}
}
function carregaPersonagem(ctx,personagem){
	var numeroCarregadas=0;
	var i=0;
 //Carrega Personagem parada
	var arrayParadasD=new Array();
	for(i=0;i<numeroImagensParado;i++){
		arrayParadasD[i]=new Image();
		arrayParadasD[i].addEventListener("load",imgLoadedHandlerPersonagem);
		arrayParadasD[i].id="paradoD"+i;
		arrayParadasD[i].src="Personagem/IdleR_"+i+".png";
	}

	var arrayParadasE=new Array();
	for(i=0;i<numeroImagensParado;i++){
		arrayParadasE[i]=new Image();
		arrayParadasE[i].addEventListener("load",imgLoadedHandlerPersonagem);
		arrayParadasE[i].id="paradoE"+i;
		arrayParadasE[i].src="Personagem/IdleL_"+i+".png";
	}

	//Carrega Personagem Andar
	var arrayCorrerD=new Array();
	for(i=0;i<numeroImagensCorrer;i++){
		arrayCorrerD[i]=new Image();
		arrayCorrerD[i].addEventListener("load",imgLoadedHandlerPersonagem);
		arrayCorrerD[i].id="correrD"+i;
		arrayCorrerD[i].src="Personagem/RunR_"+i+".png";
	}

	var arrayCorrerE=new Array();
	for(i=0;i<numeroImagensCorrer;i++){
		arrayCorrerE[i]=new Image();
		arrayCorrerE[i].addEventListener("load",imgLoadedHandlerPersonagem);
		arrayCorrerE[i].id="correrE"+i;
		arrayCorrerE[i].src="Personagem/RunL_"+i+".png";
	}

	//Carrega Personagem Morrer
	var arrayMorrerD=new Array();
	for(i=0;i<numeroImagensMorrer;i++){
		arrayMorrerD[i]=new Image();
		arrayMorrerD[i].addEventListener("load",imgLoadedHandlerPersonagem);
		arrayMorrerD[i].id="morrerD"+i;
		arrayMorrerD[i].src="Personagem/DeadR_"+i+".png";
	}

	var arrayMorrerE=new Array();
	for(i=0;i<numeroImagensMorrer;i++){
		arrayMorrerE[i]=new Image();
		arrayMorrerE[i].addEventListener("load",imgLoadedHandlerPersonagem);
		arrayMorrerE[i].id="morrerE"+i;
		arrayMorrerE[i].src="Personagem/DeadL_"+i+".png";
	}

	//Carrega Personagem Saltar
	var arraySaltarD=new Array();
	for(i=0;i<numeroImagensMorrer;i++){
		arraySaltarD[i]=new Image();
		arraySaltarD[i].addEventListener("load",imgLoadedHandlerPersonagem);
		arraySaltarD[i].id="saltarD"+i;
		arraySaltarD[i].src="Personagem/JumpR_"+i+".png";
	}

	var arraySaltarE=new Array();
	for(i=0;i<numeroImagensMorrer;i++){
		arraySaltarE[i]=new Image();
		arraySaltarE[i].addEventListener("load",imgLoadedHandlerPersonagem);
		arraySaltarE[i].id="saltarE"+i;
		arraySaltarE[i].src="Personagem/JumpL_"+i+".png";
	}

	//Carrega Personagem Deslizar
	var arrayDeslizarD=new Array();
	for(i=0;i<numeroImagensDeslizar;i++){
		arrayDeslizarD[i]=new Image();
		arrayDeslizarD[i].addEventListener("load",imgLoadedHandlerPersonagem);
		arrayDeslizarD[i].id="deslizarD"+i;
		arrayDeslizarD[i].src="Personagem/SlideR_"+i+".png";
	}

	var arrayDeslizarE=new Array();
	for(i=0;i<numeroImagensDeslizar;i++){
		arrayDeslizarE[i]=new Image();
		arrayDeslizarE[i].addEventListener("load",imgLoadedHandlerPersonagem);
		arrayDeslizarE[i].id="deslizarE"+i;
		arrayDeslizarE[i].src="Personagem/SlideL_"+i+".png";
	}


	function imgLoadedHandlerPersonagem(ev){
		var imagem=ev.target;
		var sp=new SpriteImage(0,0,Math.round(imagem.width/7),Math.round(imagem.height/7),imagem);
		sp.getDadosImagem();
		if(imagem.id.slice(0,-1)=="paradoD"){
			personagem.spArrayParadoDireita[parseInt(imagem.id.slice(-1))]=sp;
		}
		else if(imagem.id.slice(0,-1)=="paradoE"){
			personagem.spArrayParadoEsquerda[parseInt(imagem.id.slice(-1))]=sp;
		}
		else if(imagem.id.slice(0,-1)=="correrD"){
			personagem.spArrayCorrerDireita[parseInt(imagem.id.slice(-1))]=sp;
		}
		else if(imagem.id.slice(0,-1)=="correrE"){
			personagem.spArrayCorrerEsquerda[parseInt(imagem.id.slice(-1))]=sp;
		}
		else if(imagem.id.slice(0,-1)=="morrerD"){
			personagem.spArrayMorrerDireita[parseInt(imagem.id.slice(-1))]=sp;
		}
		else if(imagem.id.slice(0,-1)=="morrerE"){
			personagem.spArrayMorrerEsquerda[parseInt(imagem.id.slice(-1))]=sp;
		}
		else if(imagem.id.slice(0,-1)=="saltarD"){
			personagem.spArraySaltarDireita[parseInt(imagem.id.slice(-1))]=sp;
		}
		else if(imagem.id.slice(0,-1)=="saltarE"){
			personagem.spArraySaltarEsquerda[parseInt(imagem.id.slice(-1))]=sp;
		}
		else if(imagem.id.slice(0,-1)=="deslizarD"){
			personagem.spArrayDeslizarDireita[parseInt(imagem.id.slice(-1))]=sp;
		}
		else if(imagem.id.slice(0,-1)=="deslizarE"){
			personagem.spArrayDeslizarEsquerda[parseInt(imagem.id.slice(-1))]=sp;
		}
		numeroCarregadas++;
		if(numeroCarregadas==numeroTotalImagensPersonagem){
			var ev2 = new Event("initend");
			ctx.canvas.dispatchEvent(ev2);
		}
	}
}
function carregaInimigo(ctx,ArrayTodosInimigos){
		var numeroCarregadas=0;
		var i=0;
	 //Carrega Inimigo Correr Direita
		var arrayCorrerD=new Array();
		for(i=0;i<numeroImagensInimigo;i++){
			arrayCorrerD[i]=new Image();
			arrayCorrerD[i].addEventListener("load",imgLoadedHandlerInimigo);
			arrayCorrerD[i].id="correrD"+i;
			arrayCorrerD[i].src="Inimigo/runR"+i+".png";
		}
		var arrayCorrerE=new Array();
		for(i=0;i<numeroImagensInimigo;i++){
			arrayCorrerE[i]=new Image();
			arrayCorrerE[i].addEventListener("load",imgLoadedHandlerInimigo);
			arrayCorrerE[i].id="correrE"+i;
			arrayCorrerE[i].src="Inimigo/runL"+i+".png";
		}
		function imgLoadedHandlerInimigo(ev){
			var imagem=ev.target;
			var i;
			if(imagem.id.slice(0,-1)=="correrD"){
				for(i=0;i<ArrayTodosInimigos.length;i++){
					var sp=new SpriteImage(0,0,imagem.width,imagem.height,imagem);
					sp.getDadosImagem();
					ArrayTodosInimigos[i].spArrayCorrerDireita[parseInt(imagem.id.slice(-1))]=sp;
				}
			}
			else if(imagem.id.slice(0,-1)=="correrE"){
				for(i=0;i<ArrayTodosInimigos.length;i++){
					var sp=new SpriteImage(0,0,imagem.width,imagem.height,imagem);
					sp.getDadosImagem();
					ArrayTodosInimigos[i].spArrayCorrerEsquerda[parseInt(imagem.id.slice(-1))]=sp;
				}
			}
			numeroCarregadas++;
			if(numeroCarregadas==numeroTotalImagensInimigo){
				var ev2 = new Event("InimigosCarregados");
				ctx.canvas.dispatchEvent(ev2);
			}
		}

}
function IniciaAnimacao(ctx,personagem,tInicio,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,time){
	var al = function(time)
	{
		if(tInicio ==0){
			ArrayMusicas[2].play();
			tInicio = time;
		}
		IniciaAnimacao(ctx,personagem,tInicio,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,time);
	}
	var reqID = window.requestAnimationFrame(al);
	Renderizacao(reqID,ctx,personagem,nivelEscolhido,keyState,tInicio,time,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas);
}
function Renderizacao(reqID,ctx,personagem,nivelEscolhido,keyState,tempoInicial,time,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas){
	nivelEscolhido.desenha(time);
	var objeto;
	if((objeto=personagem.verificarColisoes(nivelEscolhido.ArrayObjetosColisao,nivelEscolhido.coordenadasComColisao,nivelEscolhido.ArrayInimigos,ctx))>=0){
			var valorReturn=nivelEscolhido.resolveColisao(objeto,personagem,ArrayMusicas);
			if(valorReturn==0){ //Nivel Concluido
					ArrayMusicas[2].pause();
					ArrayMusicas[2].currentTime=0;
					window.cancelAnimationFrame(reqID);
					IniciaFimNivel(ctx,tempoInicial,personagem,nivelEscolhido,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,0,0);
					return;
			}
	}
	if(keyState[1] && personagem.jumping==false){
				personagem.velocidadeY-=15;
				personagem.jumping=true;
	}
	if(keyState[2]){
			personagem.orientacao=true;
			personagem.velocidadeX+=0.5;
	}
	else if(keyState[0]){
			personagem.orientacao=false;
			personagem.velocidadeX-=0.5;
	}

	//Gravidade
	personagem.velocidadeY+=0.5;

	personagem.xAnterior=personagem.x;
	personagem.yAnterior=personagem.y;

	personagem.x+=personagem.velocidadeX;
	personagem.y+=personagem.velocidadeY;

	//Colisoes
	if(personagem.x<0){
		personagem.velocidadeX=0;
		personagem.x=0;
	}
	else if(personagem.x+personagem.width>ctx.canvas.width){
		personagem.velocidadeX=0;
		personagem.x=ctx.canvas.width-personagem.width;
	}

	if(personagem.y<0){
		personagem.velocidadeY=0;
		personagem.y=0;
	}
	else if(personagem.y+personagem.height>ctx.canvas.height){
		personagem.velocidadeY=0;
		personagem.y=ctx.canvas.height-personagem.height;
	}

	var tileX=Math.floor((personagem.x+personagem.width*0.4)/40);
	var tileY=Math.round((personagem.y+personagem.height)/40);
	var valorBloco=nivelEscolhido.ArrayData[tileY*numeroColunas+tileX];

	if(valorBloco!=0){
			nivelEscolhido.verificaColisaoVertical(valorBloco,personagem,tileY);
	}

	//VerificaColisaoBlocosMoveis

	nivelEscolhido.VerificaColisaoBlocosMoveis(personagem,tileX,tileY);

	var tileX=Math.floor((personagem.x+personagem.width*0.5)/40);
	var tileY=Math.floor((personagem.y+personagem.height*0.5)/40);
	var valorBloco=nivelEscolhido.ArrayData[tileY*numeroColunas+tileX];

	if(valorBloco!=0){
			nivelEscolhido.verificaColisaoHorizontal(valorBloco,personagem,tileX);
	}

	if(personagem.morreu){
		window.cancelAnimationFrame(reqID);
		AnimacaoMorte(ctx,personagem,tempoInicial,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas);
		return;
	}
	personagem.velocidadeX*=0.9;
	personagem.velocidadeY*=0.9;

	if(!keyState[0] && !keyState[1] && !keyState[2] && !keyState[3] && !personagem.jumping)
	{
			personagem.animacaoParado(time,ctx);
	}
	else if(keyState[3] && keyState[0] || keyState[3] && keyState[2]){
		if(personagem.jumping==false){
			personagem.animacaoDeslizar(time,ctx);
		}
		else{
			personagem.animacaoSaltar(time,ctx);
		}
	}
	else{
			if(personagem.jumping==false){
				personagem.animacaoCorrer(time,ctx);
			}
			else{
				personagem.animacaoSaltar(time,ctx);
			}
	}

	nivelEscolhido.animacaoBotoes(mousePosition,ArrayMusicas,0);
	if(mouseClick[0]){
		if(nivelEscolhido.ArrayPaused[1].mouseOverBoundingBox(mouseClickPosition)){
			window.cancelAnimationFrame(reqID);
			levelPaused(ctx,tempoInicial,personagem,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas);
			return;
		}
	}
	mouseClick[0]=false;
}
function AnimacaoMorte(ctx,personagem,tInicio,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,time){
	var al = function(time)
	{
		if(tInicio ==0){
			tInicio = time;
		}
		AnimacaoMorte(ctx,personagem,tInicio,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,time);
	}
	var reqID = window.requestAnimationFrame(al);
	RenderizaMorte(reqID,ctx,personagem,nivelEscolhido,keyState,tInicio,time,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas);
}

function RenderizaMorte(reqID,ctx,personagem,nivelEscolhido,keyState,tInicio,time,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas){
	nivelEscolhido.desenha();
	personagem.animacaoMorrer(time,ctx);
	if(personagem.indiceMorrer==9){
		personagem.indiceMorrer++;
		resetNivel(personagem,nivelEscolhido);
		window.cancelAnimationFrame(reqID);
		IniciaAnimacao(ctx,personagem,tInicio,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas);
		return;

	}
}
function resetNivel(personagem,nivelEscolhido){
	personagem.resetPersonagem(nivelEscolhido.xInicialPersonagem,nivelEscolhido.yInicialPersonagem);
	nivelEscolhido.resetNivel();
}

function 	canvasKeyDownHandler(ev,keyState,esquema){
	// Left - Up - Right -Down
	if(esquema==1){
		if(ev.keyCode==37){
			keyState[0]=true;
		}
		if(ev.keyCode==38){
			keyState[1]=true;
		}
		if(ev.keyCode==39){
			keyState[2]=true;
		}
		if(ev.keyCode==40){
			keyState[3]=true;
		}
	}
	else{
		if(ev.keyCode==65){
			keyState[0]=true;
		}
		if(ev.keyCode==87){
			keyState[1]=true;
		}
		if(ev.keyCode==68){
			keyState[2]=true;
		}
		if(ev.keyCode==83){
			keyState[3]=true;
		}
	}
}
function 	canvasKeyUpHandler(ev,keyState,personagem,esquema){
	if(esquema==1){
		if(ev.keyCode==37){
			keyState[0]=false;
			personagem.indiceCorrer=0;
		}
		if(ev.keyCode==38){
			keyState[1]=false;
			personagem.indiceSaltar=0;
		}
		if(ev.keyCode==39){
			keyState[2]=false;
			personagem.indiceCorrer=0;
		}
		if(ev.keyCode==40){
			keyState[3]=false;
		}
	}
	else{
		if(ev.keyCode==65){
			keyState[0]=false;
			personagem.indiceCorrer=0;
		}
		if(ev.keyCode==87){
			keyState[1]=false;
			personagem.indiceSaltar=0;
		}
		if(ev.keyCode==68){
			keyState[2]=false;
			personagem.indiceCorrer=0;
		}
		if(ev.keyCode==83){
			keyState[3]=false;
		}
	}
}

function AnimacaoMenu(tInicio,ctx,numMenu,ArrayMenus,mouseClick,mouseClickPosition,mousePosition,ArrayNiveis,ArrayMusicas,esquema){
	var al = function(time)
	{
		if(tInicio ==0){
			tInicio = time;
		}
		AnimacaoMenu(tInicio,ctx,numMenu,ArrayMenus,mouseClick,mouseClickPosition,mousePosition,ArrayNiveis,ArrayMusicas,esquema);
	}
	var reqID = window.requestAnimationFrame(al);
	RenderizaMenu(reqID,ctx,numMenu,ArrayMenus,mouseClick,mouseClickPosition,mousePosition,ArrayNiveis,ArrayMusicas,esquema);
}

function RenderizaMenu(reqID,ctx,numMenu,ArrayMenus,mouseClick,mouseClickPosition,mousePosition,ArrayNiveis,ArrayMusicas,esquema){
	ArrayMenus[numMenu[0]].apaga();
	ArrayMenus[numMenu[0]].desenha(numMenu[0],ArrayNiveis);
	if(mouseClick[0]){
			ArrayMenus[numMenu[0]].ResolveMouseClick(numMenu,reqID,ctx,ArrayMusicas,ArrayMenus,mouseClickPosition,esquema);
	}
	ArrayMenus[numMenu[0]].animacaoBotoes(mousePosition,ArrayMusicas);
	mouseClick[0]=false;
}

function IniciaFimNivel(ctx,tInicio,personagem,nivelEscolhido,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,auxiliar,tempoTotal,time){
	var al = function(time)
	{
		if(tInicio ==0){
			tInicio = time;
		}
		if(auxiliar==0){
			tempoTotal=time-tInicio;
		}
		IniciaFimNivel(ctx,tInicio,personagem,nivelEscolhido,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,1,tempoTotal,time);
	}
	var reqID = window.requestAnimationFrame(al);
	RenderizaFimNivel(reqID,ctx,personagem,nivelEscolhido,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,tempoTotal);
}
function RenderizaFimNivel(reqID,ctx,personagem,nivelEscolhido,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,tempoTotal){

	nivelEscolhido.DesenhaFimNivel();
	var score=nivelEscolhido.pontuacao(tempoTotal);
	ctx.font="30px Verdana";
	ctx.fillText(score,360,320);
	nivelEscolhido.animacaoBotoes(mousePosition,ArrayMusicas,2);

	if(mouseClick[0]){
			if(nivelEscolhido.ArrayLevelCompleted[1].mouseOverBoundingBox(mouseClickPosition)){
				window.cancelAnimationFrame(reqID);
				nivelEscolhido.atualizaPontuacao(score);
				resetNivel(personagem,nivelEscolhido);
				var ev2 = new Event("initend");
				ctx.canvas.dispatchEvent(ev2);
			}
	}
	mouseClick[0]=false;
}

function levelPaused(ctx,tInicio,personagem,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,time){
	var al = function(time)
	{
		if(tInicio ==0){
			tInicio = time;
		}
		levelPaused(ctx,tInicio,personagem,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,time);
	}
	var reqID = window.requestAnimationFrame(al);
	RenderizaPausa(reqID,ctx,tInicio,personagem,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,time);
}
function RenderizaPausa(reqID,ctx,tInicio,personagem,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,time){
	nivelEscolhido.DesenhaPausa();
	if(mouseClick[0]){
		if(nivelEscolhido.ArrayPaused[2].mouseOverBoundingBox(mouseClickPosition)){
			window.cancelAnimationFrame(reqID);
			mouseClick[0]=false;
			var tempo=time-tInicio;
			ArrayMusicas[2].play();
			IniciaAnimacao(ctx,personagem,tInicio,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas,tempo);
			return;
		}
		if(nivelEscolhido.ArrayPaused[3].mouseOverBoundingBox(mouseClickPosition)){
			window.cancelAnimationFrame(reqID);
			mouseClick[0]=false;
			resetNivel(personagem,nivelEscolhido);
			IniciaAnimacao(ctx,personagem,0,nivelEscolhido,keyState,mouseClickPosition,mouseClick,mousePosition,ArrayMusicas);
			return;
		}
		if(nivelEscolhido.ArrayPaused[4].mouseOverBoundingBox(mouseClickPosition)){
			window.cancelAnimationFrame(reqID);
			ArrayMusicas[2].pause();
			ArrayMusicas[2].currentTime=0;
			resetNivel(personagem,nivelEscolhido);
			mouseClick[0]=false;
			var ev2 = new Event("initend");
			ctx.canvas.dispatchEvent(ev2);
		}
	}
	nivelEscolhido.animacaoBotoes(mousePosition,ArrayMusicas,1);
	mouseClick[0]=false;
}
