"use strict"

class Inimigo{
  constructor(xInicial,yInicial,width,height,distancia,orientacao,velocidade){
    this.xInicial=xInicial;
    this.yInicial=yInicial;;
    if(!orientacao){
      this.limiteInferiorX=xInicial-distancia;
      this.limiteSuperiorX=xInicial;
    }
    else{
      this.limiteSuperiorX=xInicial+distancia;
      this.limiteInferiorX=xInicial;
    }
    this.x=xInicial;
    this.y=yInicial;

    this.width=width;
    this.height=height;

    this.distancia=distancia;
    this.orientacao=orientacao;
    this.velocidade=velocidade;

    this.spArrayCorrerDireita=new Array();
    this.spArrayCorrerEsquerda=new Array();

    this.tempoAnimacao=0;

    this.indiceCorrer=0;
    this.vivo=true;
  }
  moveInimigo(){
    if(this.orientacao){ //TRUE = Direita FALSE = Esquerda
        this.x+=this.velocidade;
        if(this.x>this.limiteSuperiorX){
          this.orientacao=false;
        }
    }
    else{
        this.x-=this.velocidade;
        if(this.x<this.limiteInferiorX){
          this.orientacao=true;
        }
    }
  }

  animacaoCorrer(time,ctx){
    if(!this.vivo){
      return;
    }
    if(time-this.tempoAnimacao>100){
        if(this.indiceCorrer==2){
            this.indiceCorrer=0;
        }else{
            this.indiceCorrer++;
        }
        this.tempoAnimacao=time;
    }
    if(this.orientacao){
  			this.spArrayCorrerDireita[this.indiceCorrer].x=this.x
  			this.spArrayCorrerDireita[this.indiceCorrer].y=this.y
  			this.spArrayCorrerDireita[this.indiceCorrer].draw(ctx);
  	}
  	else{
  		  this.spArrayCorrerEsquerda[this.indiceCorrer].x=this.x
  			this.spArrayCorrerEsquerda[this.indiceCorrer].y=this.y
  			this.spArrayCorrerEsquerda[this.indiceCorrer].draw(ctx);
  	}
    this.moveInimigo();
	}
  getSprite(){
    if(this.orientacao){
      return this.spArrayCorrerDireita[this.indiceCorrer];
    }
    else{
      return this.spArrayCorrerEsquerda[this.indiceCorrer];
    }
  }
  resetInimigo(){
    this.x=this.xInicial;
    this.y=this.yInicial;

    this.tempoAnimacao=0;

    this.indiceCorrer=0;
    this.vivo=true;
  }

}
